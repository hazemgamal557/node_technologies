<?php

Route::get('/api/test-sms', function () {
    $phone = '+966566843000';

    $user = env('QADAT_USER');
    $sender = env('QADAT_SENDER');
    $password = env('QADAT_PASSWORD');

    $message = urlencode("Verification code is: 1234");
    $queryString = "username={$user}&password={$password}&message={$message}&numbers={$phone}&sender={$sender}&unicode=u&return=Json";

    $connection = curl_init();
    curl_setopt($connection, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; U; Linux i686; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18');
    curl_setopt($connection, CURLOPT_URL, "http://www.qyadat.com/sms/api/sendsms.php?{$queryString}");
    curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);

    
    try {
        $response = curl_exec($connection);
        curl_close($connection);
        return $response;
        \Log::info('success: sms response', [
            'response' => $response,
        ]);
    } catch (\Exception $e) {
        \Log::alert('fail: sms response', [
            'response_error' => $e->getMessage(),
        ]);
    }
});
