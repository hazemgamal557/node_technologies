<?php

use Illuminate\Http\Request;

# ------------------------------------------------------------------------ #
#                               Authentication                             #
# ------------------------------------------------------------------------ #

Route::prefix('auth')->group(function () {
    # ------------------- User Register ----------------------------#
    Route::post('/register-and-send-confirm-code', 'AuthController@registerAndSendConfirmationCode');
    Route::post('/activate-registered-user', 'AuthController@activateRegisteredUser');

    # ------------------- User Login ----------------------------#
    Route::post('/request-login', 'AuthController@requestLogin');
    Route::post('/login', 'AuthController@login');

    # ------------------- User resend-confirmation -----------------------#
    Route::post('/resend-confirm-code', 'AuthController@resendConfirmationCode');

    #------------------------------ Superadmin ----------------------------#
    Route::post('/superadmin/login', 'Admin\SuperadminAuthController@login');

    #------------------------------ Logged in ----------------------------#
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::post('/send-update-phone-confirm-code', 'AuthController@sendUpdatePhoneConfirmationCode');
        Route::post('/resend-update-confirm_code', 'AuthController@resendUpdateConfirmationCode');
        Route::post('/check-confirm-code-and-update-phone', 'AuthController@checkConfirmationCodeAndUpdatePhone');
        Route::post('/update-image-and-address', 'AuthController@updateNameAddressAndImage');
        Route::get('/show-profile', 'AuthController@ShowProfile');
        Route::post('/update-user-lang', 'AuthController@setLang');
        Route::post('/logout', 'AuthController@logout');

        #------------------------------ Superadmin ----------------------------#
        Route::get('/superadmin/me/', 'Admin\SuperadminAuthController@me');
        Route::post('/superadmin/logout', 'Admin\SuperadminAuthController@logout');
    });
});

// Website products
Route::get('/products', 'ProductController@products');

// ------------------------------------ User un auth routes -------------------------------------------

Route::prefix('user')->group(function () {
    // ----------------------- Category --------------------\\
    Route::get('/all/categories', 'CategoryController@index');
    Route::get('/single/category/{id}', 'CategoryController@show');
    Route::get('/subCat/all', 'SubCategoryController@index');
    Route::get('/Single/subCat/{subCategory}', 'SubCategoryController@show');
    // ----------------------- productus ------------------------ \\
    Route::prefix('product')->group(function () {
        Route::get('/all', 'ProductController@index');
        Route::get('/recommend-product/{id}', 'ProductController@recommendProduct');
    });
    Route::prefix('slider')->group(function() {
        Route::get('/all', 'SliderController@index');
    });
    // ----------------   favourite ------------//
    Route::prefix('favourite')->group(function () {
        Route::middleware(['auth:sanctum'])->group(function () {
            Route::post('/add/{id}', 'FavouriteController@add');
            Route::get('/all', 'FavouriteController@show');
        });
    });
    // ---------------- End Favourite ---------- //
    // ---------------- Setting  ----------------------------- //
    Route::prefix('setting')->group(function () {
        Route::get('/socialLinks', 'SettingController@retriveSocialMedia');
        Route::get('/pages/{key}', 'SettingController@getPage');

    });
    // ---------------- End Setting  ----------------------------- //
    // ------------------- OrderDetails -----------//
    Route::prefix('orders')->group(function () {
        Route::middleware(['auth:sanctum'])->group(function () {
            Route::get('/get-All-Orders', 'OrderController@getAllOrders');
            Route::get('/get-ordered-order', 'OrderController@getOrderWhereNotCard');
            Route::post('/add-to-cart/{id}', 'OrderController@AddToOrderDetail');
            Route::delete('/delete-from-cart/{id}', 'OrderController@deleteOrderDetail');
            Route::post('/update-order-detail/{id}', 'OrderController@updateOrderDetail');
            Route::post('/update-card-quantity/{id}', 'OrderController@updateOrderDetailQuantity');
        });
    });
    // ------------------- End OrderDetails -----//
    Route::post('/contact-us', 'ContactUsController@store');



    #------------ Update user lang , device & is_notificationable -------#
    Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/set-device', 'UserController@setDeviceId');
    // Route::post('/set-lang', 'UserController@setLang');
    // Route::post('/set-notificationable', 'UserController@setNotificationable');
    // Route::get('/notification', 'NotificationController@index');
    // Route::post('/notification/mark-as-read', 'NotificationController@markAsRead');
    });
});

// ------------------------------------ Contact us -------------------------------------------

# ------------------------------------------------------------------------ #
#                              Superadmin routes                           #
# ------------------------------------------------------------------------ #
Route::group(['middleware' => ['auth:sanctum', 'admin'], 'prefix' => 'admin'], function () {

    #---------------------------- Admin --------------------------------#
    Route::get('/', 'Admin\AdminController@index');
    Route::post('/', 'Admin\AdminController@store');
    Route::post('/update-my-profile', 'Admin\AdminController@updateProfile');
    Route::delete('/{id}', 'Admin\AdminController@remove');

    #---------------------------- Statistics --------------------------------#
    Route::get('/count', 'Admin\SettingController@count');

    #------------------------------- Users --------------------------------#
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'Admin\UserController@index');
        Route::get('/find/all', 'Admin\UserController@indexAll');
        Route::get('/{id}', 'Admin\UserController@show');
        Route::post('/{id}/block', 'Admin\UserController@block');
        Route::post('/{id}/unblock', 'Admin\UserController@unblock');
    });

    #---------------------------- Order --------------------------------#
    Route::group(['prefix' => 'order'], function () {
        Route::get('/', 'Admin\OrderController@index');
        Route::post('/{id}/activate', 'Admin\OrderController@activate');
        Route::post('/{id}/expire', 'Admin\OrderController@expire');
        Route::post('/{id}/cart', 'Admin\OrderController@cart');
        Route::get('/{id}', 'Admin\OrderController@orderDetails');
    });

    #---------------------------- Category --------------------------------#
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'Admin\CategoryController@index');
        Route::get('/all', 'Admin\CategoryController@indexAll');
        Route::post('/add', 'Admin\CategoryController@store');
        Route::post('/change-status', 'Admin\CategoryController@UpdateStatus');
        Route::post('/update-sort/{category}', 'Admin\CategoryController@updateCategorySort');
        Route::post('/{category}/update', 'Admin\CategoryController@update');
        Route::delete('/{category}/delete', 'Admin\CategoryController@destroy');
    });

    #---------------------------- SubCategory --------------------------------#
    Route::group(['prefix' => 'subcategory'], function () {
        Route::get('/{id}', 'Admin\SubCategoryController@index');
        Route::get('/find/all', 'Admin\SubCategoryController@indexAll');
        Route::post('/add', 'Admin\SubCategoryController@store');
        Route::post('/change-status', 'Admin\SubCategoryController@UpdateStatus');
        Route::post('/{subcategory}/update', 'Admin\SubCategoryController@update');
        Route::delete('/{subcategory}/delete', 'Admin\SubCategoryController@destroy');
    });
     #---------------------------- Slider --------------------------------#
     Route::group(['prefix' => 'slider'], function(){
         Route::get('/all', 'Admin\SliderController@index');
         Route::post('/add', 'Admin\SliderController@add');
         Route::delete('/delete/{slider}', 'Admin\SliderController@destroy');
     });
    # ------------------- Products ----------------- #
    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'Admin\ProductController@index');
        Route::get('/all', 'Admin\ProductController@indexAll');
        Route::get('/{id}', 'Admin\ProductController@show');
        Route::post('/', 'Admin\ProductController@store');
        Route::post('/{id}', 'Admin\ProductController@update');
        Route::delete('/{id}', 'Admin\ProductController@remove');

        # ------------------- Products media ----------------- #
        Route::post('/{id}/attach-image', 'Admin\ProductController@attachProductImage');
        Route::delete('/deattach-image/{id}', 'Admin\ProductController@removeProductImage');

    });

    #----------------------------- Settings ------------------------------#
    Route::post('/settings/{id}/update', 'Admin\SettingController@update');
    Route::get('/socialLinks', 'Admin\SettingController@retriveSocialMedia');
    Route::get('/pages/{key}', 'Admin\SettingController@getPage');
    Route::put('/pages/{key}', 'Admin\SettingController@updatePage');

    #----------------------------- Contact us ----------------------------#
    Route::group(['prefix' => 'contact-us'], function () {
        Route::get('/', 'Admin\ContactUsController@getMessages');
        Route::post('/{id}/mark-as-seen', 'Admin\ContactUsController@markAsSeen');
        Route::delete('/{id}', 'Admin\ContactUsController@remove');
    });

   #----------------------------- Notifications ----------------------------#
   Route::get('/notification', 'Admin\NotificationController@index');
   Route::post('/send-notification', 'Admin\NotificationController@send');
   Route::delete('/notification/{id}/delete', 'Admin\NotificationController@delete');
});
