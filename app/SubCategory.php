<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = ['name_ar','name_en', 'image', 'category_id', 'show'];
    protected $casts = ['show' => 'boolean'];
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function product()
    {
        return $this->hasMany(Product::class);
    }
    public function getImageAttribute()
    {
        if (empty($this->attributes['image'])) {
            return asset('storage/subcategory_temp/default.png');
        }else{
            return asset("storage/{$this->attributes['image']}");
        }
    }

}
