<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name_en', 'name_ar', 'image', 'show'];
    protected $casts = ['show' => 'boolean'];

    public function subCategories()
    {
        return $this->hasMany(SubCategory::class, 'category_id');
    }
    public function getImageAttribute()
    {
        if (empty($this->attributes['image'])) {
            return asset('storage/default/category/default.png');
        }else{
            return asset("storage/{$this->attributes['image']}");
        }
    }
}
