<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name_ar',
        'name_en',
        'type_ar',
        'type_en',
        'trademark_ar',
        'trademark_en',
        'price',
        'description_ar',
        'is_card',
        'description_en',
        'sub_categories_id',
    ];

    public function media()
    {
        return $this->hasMany('App\ProductMedia');
    }
    public function favourites()
    {
        return $this->hasMany(Favourite::class, 'product_id');

    }
    public function orderDetail()
    {
        return $this->hasMany(OrderDetails::class, 'product_id');
    }
    public function subCategory()
    {
        return $this->belongsTo('App\SubCategory', 'sub_categories_id');

    }
    public function getImageAttribute()
    {
        if (empty($this->attributes['image'])) {
            return asset('storage/default/product/default.png');
        } else {
            return asset("storage/{$this->attributes['image']}");
        }
    }
}
