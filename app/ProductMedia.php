<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMedia extends Model
{
    protected $fillable = ['image', 'caption', 'product_id'];
    public $timestamps = false;

    public function getImageAttribute()
    {
        if (empty($this->attributes['image'])) {
            return asset('storage/default/product/default.png');
        }

        return strpos($this->attributes['image'] ,'http') !== FALSE
            ? $this->attributes['image']
            : asset("storage/{$this->attributes['image']}");
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
