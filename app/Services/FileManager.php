<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileManager
{
    public function uploadCategoryImage(UploadedFile $uploadedFile, String $prevFileName = '')
    {
        return $this->uploadFixedSizeImage($uploadedFile, $prevFileName, 'store', 'stores', 600, 500);

    }
    public function uploadSliderImage(UploadedFile $uploadedFile, String $prevFileName = '')
    {
        return $this->uploadFixedSizeImage($uploadedFile, $prevFileName, 'slider', 'sliders', 600, 500);

    }

    /**
     * upload new or update prev list of product-images.
     *
     * @param  array[UploadedFile] $images
     * @param  array[string]       $prevImages
     * @return string              Filenames that will be stored in the db
     */
    public function uploadProductImages(array $images, array $prevImages = [])
    {
        foreach ($prevImages as $image) {
            $this->remove($image);
        }

        foreach ($images as $image) {
            $imagesNames[] = $this->uploadProductImage($image);
        }

        return $imagesNames;
    }

    /**
     * upload new or update prev product-image.
     *
     * @param  UploadedFile $uploadedFile
     * @param  string       $prevFileName
     * @return string       the file name that will be stored in the db
     */
    public function uploadProductImage(UploadedFile $uploadedFile, string $prevFileName = '')
    {
       return $this->uploadFixedSizeImage($uploadedFile, $prevFileName, 'product', 'products');
    }


    public function uploadUserImage(UploadedFile $uploadedFile, string $prevFileName = '')
    {
       return $this->uploadFixedSizeImage($uploadedFile, $prevFileName, 'user', 'users');
    }


    private function uploadFixedSizeImage(UploadedFile $uploadedFile, string $prevFileName, string $prefix, string $directory)
    {
        // delete the previous file if exists.
        if (!empty($prevFileName) && is_file( storage_path("app/public/{$prevFileName}") )) {
           $this->remove($prevFileName);
        }

        // generate the new file name.
        $newFileName =
            "{$prefix}-"
            . uniqid(time() . '-', true)
            . substr(pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME), 0, 10)
            . '.'
            . $uploadedFile->getClientOriginalExtension();

        // prepare the image file size
        $streamFileImage = \Image::make($uploadedFile)->stream();

        // store the file
        Storage::put("public/{$directory}/{$newFileName}", $streamFileImage);
        return "{$directory}/$newFileName";
    }

    public function remove(string $file)
    {
        Storage::delete("public/{$file}");
    }

}
