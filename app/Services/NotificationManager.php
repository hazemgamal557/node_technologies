<?php

namespace App\Services;

use FCM;
use App\Models\User;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class NotificationManager
{
    /**
     * send a FCM notification to the given tokens.
     *
     * @param  array  $tokens
     * @param  string $body
     * @param  string $title
     * @return void
     */
    public function send(array $tokens, string $body, array $payload = [], string $title = 'NodeTechnology')
    {
        if (count($tokens) == 0) return;

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder
            ->setSound('sound')
            ->setBody($body);

        $dataBuilder = new PayloadDataBuilder();
        foreach ($payload as $attribute) {
            $dataBuilder->addData($attribute);
        }

        try {
            FCM::sendTo(
                $tokens,
                $optionBuilder->build(),
                $notificationBuilder->build(),
                $dataBuilder->build()
            );
        }
        catch (\Throwable $th) {}
    }
}
