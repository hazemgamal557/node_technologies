<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $fillable = [
        'order_id',
        'price',
        'quantity',
        'product_name_ar',
        'product_name_en'
    ];

    function product(){
        return $this->belongsTo('App\Product')->withTrashed();

    }

    function order(){
        return $this->belongsTo('App\Order');

    }

}
