<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['image'];

    public function getImageAttribute()
    {
        if (empty($this->attributes['image'])) {
            return asset('storage/default/slider/default.png');
        }else{
            return asset("storage/{$this->attributes['image']}");
        }
    }

}
