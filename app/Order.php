<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'status',
        'user_id',

    ];

    function orderDetail()
    {
        return $this->hasMany("App\OrderDetails");
    }
    // function product()
    // {
    //     return $this->belongsToMany("App\Product" ,"App\OrderDetails");
    // }
    public function user()
    {
        return $this->belongsTo("App\User");
    }
}
