<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(Request $request)
    {
        $input = $request->validate([
            'id' => 'numeric|exists:products,id',
            'q' => 'string',
            'limit' => 'integer|min:1',
            'sub_categories_id' => 'numeric|exists:sub_categories,id',
            'category_id' => 'numeric|exists:categories,id',
        ]);

        $productsQuery = Product::with([
            'subCategory',
            'media',
        ])
            ->orderBy('id', 'DESC');

        if (!empty($input['q'])) {
            $productsQuery->where(function ($query) use ($input) {
                $query->where('name_ar', 'LIKE', "%{$input['q']}%")
                    ->orWhere('name_en', 'LIKE', "%{$input['q']}%")
                    ->orWhere('type_ar', 'LIKE', "%{$input['q']}%")
                    ->orWhere('type_en', 'LIKE', "%{$input['q']}%")
                    ->orWhere('description_ar', 'LIKE', "%{$input['q']}%")
                    ->orWhere('description_ar', 'LIKE', "%{$input['q']}%");
            });
        }

        if (!empty($input['id'])) {
            $productsQuery->where('id', $input['id']);
        }

        if (!empty($input['sub_categories_id'])) {
            $productsQuery->where('sub_categories_id', $input['sub_categories_id']);
        }

        if (!empty($input['category_id'])) {
            $productsQuery->whereHas('subCategory', function ($query) use ($input) {
                $query->where('category_id', $input['category_id']);
            });
        }

        $authGuard = \Auth::guard('sanctum');
        if ($authGuard->check()) {
            $productsQuery->leftJoin('favourites', function ($join) use ($authGuard) {
                $join->on('products.id', '=', 'favourites.product_id')
                    ->where('favourites.user_id', $authGuard->id());
            })->selectRaw('count(DISTINCT favourites.user_id) AS is_favourite, products.*')
                ->groupBy('products.id');
        }

        if ($authGuard->check()) {
            $productsQuery->leftJoin('order_details', function ($join) use ($authGuard) {
                $join->on('products.id', '=', 'order_details.product_id');
            })
                ->leftJoin('orders', function ($join) use ($authGuard) {
                    $join->on('order_details.order_id', '=', 'orders.id')
                        ->where('orders.user_id', $authGuard->id())
                        ->where('orders.status', 'cart');
                })
                ->selectRaw('count(DISTINCT orders.user_id) AS is_card, products.*')
                ->groupBy('products.id');
        }
        $myrecord = $productsQuery->paginate(
            $request->query('limit', 10)
        );
        $products = ProductResource::collection($myrecord);
        return response()->json($products);
    }

    public function recommendProduct(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $recommendProduct = Product::where('sub_categories_id', $product->sub_categories_id)
            ->where('products.id', '!=', $product->id)->orderBy('id', 'desc');

        $authGuard = \Auth::guard('sanctum');
        if ($authGuard->check()) {
            $recommendProduct->leftJoin('favourites', function ($join) use ($authGuard) {
                $join->on('products.id', '=', 'favourites.product_id')
                    ->where('favourites.user_id', $authGuard->id());
            })->selectRaw('count(DISTINCT favourites.user_id) AS is_favourite, products.*')
                ->groupBy('products.id');
        }
        $recommended = $recommendProduct->paginate($request->query('limit', 10));

        $products = ProductResource::collection($recommended);

        return response()->json($products);

    }

    public function products()
    {
        return Product::
            with('media')
            ->orderBy('id', 'desc')
            ->take(3)
            ->get();
    }

}
