<?php

namespace App\Http\Controllers\Admin;

use App\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    /**
     * list all messages
     * 
     * @param int $_GET['page']
     * @param int $_GET['limit']
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getMessages(Request $request)
    {
        $query =  ContactUs::orderBy('id', 'DESC');

        if ($request->query('status', null) && $request->query('status') == 'unseen') {
            $query->where('seen', 0);
        }

        return $query->paginate(
                $request->query('limit', 10)
            );
    }

    /**
     * marke the given contact-us message id as seen
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function markAsSeen(Request $request, int $id)
    {
        $message = ContactUs::findOrFail($id);
        $message->seen = true;
        $message->save();

        return response()->json([
            'message' => 'the message has been updated successfully'
        ], 201);
    }

    /**
     * delete the given contact-us id
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, int $id)
    {
        $contactUs = ContactUs::findOrFail($id);
        $contactUs->forceDelete();

        return response()->json([
            'message' => 'the message has been deleted successfully'
        ], 201);
    }
}
