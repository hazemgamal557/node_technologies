<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Services\FileManager;
use App\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function index(Request $request, $id)
    {
        $records = SubCategory::where('category_id',$id)->with('category')->orderBy('id', 'desc')
            ->paginate(
                $request->query('limit', 3)
            );
        return $records;
    }

    public function indexAll()
    {
        $records = SubCategory::with('category')->orderBy('id', 'desc')->get();
        return $records;
    }

    public function store(Request $request, FileManager $fileManager)
    {

        $request->validate([
            'name_ar' => 'required|string',
            'name_en' => 'required|string',
            'image'   => 'image|mimes:jpeg,jpg,png|max:20000'
        ]);



        $subCategory = new SubCategory;
        $subCategory->name_en = $request->name_en;
        $subCategory->name_ar = $request->name_ar;
        $subCategory->category_id = $request->category_id;
        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $subCategory->image = $fileManager->uploadCategoryImage(request()->file('image'));
        }
        $subCategory->save();

        return response()->json([
            'message' => __('messages.category-has-been-added'),
        ], 201);
    }

    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    public function update(Request $request, FileManager $fileManager, int $id)
    {
        $category = SubCategory::findOrFail($id);
        $category->name_en = $request->input('name_en');
        $category->name_ar = $request->input('name_ar');
        $category->category_id = $request->input('category_id');

        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $category->image = $fileManager->uploadCategoryImage(request()->file('image'));
        }

        $category->update();

        return response()->json([
            'message' => __('messages.sub-category-has-been-updated'),
        ], 200);
    }

    public function UpdateStatus(Request $request)
    {
        $data = $request->validate([
            'subcategory_id' => 'required|numeric|exists:sub_categories,id',
            'show' => 'required|bool'
        ]);

        $subCategory = SubCategory::find($data['subcategory_id']);
        $subCategory->update([
            'show' => $data['show']
        ]);
        return $subCategory;
    }

    public function destroy($id)
    {
        $subcategory = SubCategory::findOrFail($id);
        $subcategory->delete();

        return response()->json([
            'message' => __('messages.sub-category-has-been-deleted'),
        ], 201);
    }
}
