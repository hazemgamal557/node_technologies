<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notification;
use App\Services\NotificationManager;
use App\User;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * send notification to all|specific user
     *
     * @param int $_GET['page']
     * @param int $_GET['limit']
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        $notifications = Notification::orderBy('id', 'desc')
            ->paginate(
                $request->query('limit', 10)
            );

        // $notification = NotificationResource::collection($notifications);

        return response()->json($notifications);
    }

    public function send(Request $request, NotificationManager $notificationManager)
    {
        $data = $request->validate([
            'target' => [
                'required',
                'string',
                'regex:/^(all|player|club|extra)$/',
            ],
            'message_ar' => 'required|string',
            'message_en' => 'required|string'
        ]);
	$users = User::where("is_notificationable", true)
               ->select(['id', 'device_id', 'lang'])
		->get();

 foreach ($users as $user) {
            Notification::create([
                'message_ar' => $data['message_ar'],
                'message_en' => $data['message_en'],
                'user_id' => $user->id,
            ]);
        }



        $usersQuery = User::select(['id', 'device_id', 'lang']);
        $users = $usersQuery->get();
        foreach(['ar', 'en'] as $lang) {
            $tokens = $users->whereNotNull('device_id')
                ->where('lang', $lang)
                ->pluck('device_id')
                ->toArray();

            $notificationManager->send(
                $tokens,
                $data["message_{$lang}"]
            );
        }


        return response()->json([
            'message' => 'the notifications have been sent successfully'
        ]);

    }

    public function delete($id)
    {
        $notification = Notification::findOrFail($id);
        $notification->delete();

        return response()->json([
            'message' => 'notification has been deleted',
        ]);
    }

}
