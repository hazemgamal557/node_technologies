<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
Use App\Admin;
use Illuminate\Http\Request;

class SuperadminAuthController extends Controller
{
    public function login(Request $request)
    {
        $data = $request->validate([
            'email'     => 'required',
            'password'  => 'required'
        ]);

        // attempt
        $admin = Admin::where('email', $data['email'])->first();

        // authentication failure
        if (is_null($admin) || !\Hash::check($data['password'], $admin->password)) {
            return response()->json([
                'message' => 'authentication failure'
            ], 401);
        }

        return response()->json([
            'admin'   => $admin,
            'token'   => $admin->createToken('admin-token' ,['server:update'])->plainTextToken,
            'message' => 'admin has been logged in successfully'
        ], 200);
    }

    public function me()
    {
        return response()->json([
            'admin' => \Auth::user()
        ]);
    }
    public function logout()
    {
        // dd( \Auth::user());
        \Auth::user()->tokens()->delete();

        return response()->json([
            'admin'   => \Auth::user(),
            'message' => 'admin has been logged out successfully'
        ], 200);
    }    


}
