<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{/**
 * list all admins
 *
 * @param int $_GET['page']
 * @param int $_GET['limit']
 *
 * @param  \Illuminate\Http\Request $request
 * @return \Illuminate\Http\Response
 */
    public function index(Request $request)
    {
        // $this->checkSuperAdmin();

        return Admin::paginate(
            $request->query('limit', 10)
        );
    }

    /**
     * storing a new admin
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->checkSuperAdmin();

        $data = $request->validate([
            'name' => 'required|string|min:2',
            'email' => 'required|email|unique:admins,email',
            'password' => 'required|string|min:5',
        ]);

        $data['password'] = \Hash::make($data['password']);
        $admin = Admin::create($data);

        return response()->json([
            'record' => $admin,
            'message' => 'the admin has been created successfully',
        ], 201);
    }

    /**
     * delete the given admin id
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, int $id)
    {
        $this->checkSuperAdmin();

        $admin = Admin::findOrFail($id);
        $admin->forceDelete();

        return response()->json([
            'message' => 'the admin has been deleted successfully',
        ], 201);
    }

    /**
     * update the auth admin profile
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $data = $request->validate([
            'name' => 'string|min:2',
            'password' => 'string|min:5',
            'email' => 'email|unique:admins,email,' . \Auth::id(),
        ]);

        $admin = \Auth::user();

        if (!empty($data['name'])) {
            $admin->name = $data['name'];
        }

        if (!empty($data['email'])) {
            $admin->email = $data['email'];
        }

        if (!empty($data['password'])) {
            $admin->password = Hash::make($data['password']);
        }

        $admin->save();

        return response()->json([
            'admin' => $admin,
            'message' => 'admin profile has been updated successfully',
        ], 201);
    }

    /**
     * currenty super dmin is admin@admin.com of id=1
     *
     * @throws \Illuminate\Validation\UnauthorizedException;
     * @return void
     */
    private function checkSuperAdmin()
    {
        if (\Auth::id() == 1) {
            return;
        }

        throw new \Illuminate\Validation\UnauthorizedException;
    }
}
