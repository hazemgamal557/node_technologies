<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetails;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $input = $request->validate([
            'id' => 'numeric|exists:products,id',
            'q' => 'string',
            'limit' => 'integer|min:1',
            'selected' => 'string',
        ]);

        $ordersQuery = Order::where('status', '!=', 'cart')->with('user')->orderBy('id', 'DESC');

        if (!empty($input['q'])) {
            $ordersQuery->whereHas('user', function ($query) use ($input) {
                $query->where('name', 'LIKE', "%{$input['q']}%");
            });
        }

        if (!empty($input['selected'])) {
            if ($input['selected'] == 2) {
                $ordersQuery->where('status', 'ordered');
            } else {
                $ordersQuery->where('status', 'delivered');
            }
        }

        return $ordersQuery->paginate(
            $request->query('limit', 10)
        );
    }

    public function activate($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 'ordered';
        $order->update();

        return response()->json('Item activated');
    }

    public function expire($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 'delivered';
        $order->update();

        return response()->json(' delivered');
    }

    public function orderDetails(Request $request, $id)
    {
        // cart count
        $totalPrice = 0;
        $count = 0;
        $orderD = OrderDetails::where('order_id', $id)->get();

        foreach ($orderD as $order) {
            $count += $order->quantity;
            $totalPrice += $order->quantity * $order->product->price;
        }

        $orderDetails = OrderDetails::where('order_id', $id)->with(['product.media'])->paginate(
            $request->query('limit', 10)
        );

        $totalPrice = round($totalPrice, 2);
        return [$orderDetails, $count, $totalPrice];
    }
}
