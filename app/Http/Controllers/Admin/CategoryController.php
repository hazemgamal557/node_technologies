<?php


namespace App\Http\Controllers\Admin;
use App\Services\FileManager;
use App\Http\Requests\CategoryRequest;
use App\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $records = Category::orderBy('sort', 'asc')
            ->paginate(
                $request->query('limit', 10)
            );
        return $records;
    }


    /** Retrieve all categories for select box */
    public function indexAll(Request $request)
    {
        $records = Category::orderBy('id', 'desc')->get();

        return $records;
    }



    public function store(Request $request, FileManager $fileManager)
    {

        $request->validate([
            'name_en' => 'required|string|max:45',
            'name_ar' => 'required|string|max:45',
            'image'   => 'image|mimes:jpeg,jpg,png|max:20000'
        ]);


        $category = new Category;
        $category->name_ar = $request->input('name_ar');
        $category->name_en = $request->input('name_en');

        if ($request->hasFile('image')  && request()->file('image')->isValid()) {
            $category->image = $fileManager->uploadCategoryImage(request()->file('image'));
        }

        $categorySort = Category::orderBy('sort', 'desc')->first();
        // dd($categorySort->sort);
        if(!is_null($categorySort))
        {
            $category->sort = $categorySort->sort + 1;
        }else{
            $category->sort = 1;
        }
        $category->save();

        return response()->json([
            'message' => __('messages.category-has-been-added')
        ], 201);
    }

    public function updateCategorySort(Request $request, int $id)
    {
        /**
         * 1- red
         * 3- blue
         * 2- green
         */
       $data = $request->validate([
            'sort'   => 'required|numeric|integer'
        ]);
        $category = Category::findOrFail($id);
        $existsSort = Category::where('sort', $data['sort'])->first();
        if(!is_null($existsSort)){
            $existsSort->sort = $category->sort;
            $existsSort->save();
            $category->sort = $data['sort'];
            // $category->save();
        }else{
            $category->sort = $data['sort'];
           
        }
        $category->save();
        return response()->json([
            'message' => 'send-suceess'
        ]);
        // dd($category->id);
    }

    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    public function update(CategoryRequest $request, FileManager $fileManager, int $id)
    {
        $category = Category::findOrFail($id);
        $category->name_ar = $request->input('name_ar');
        $category->name_en = $request->input('name_en');

        if ($request->hasFile('image')  && request()->file('image')->isValid()) {
            $category->image = $fileManager->uploadCategoryImage(request()->file('image'));
        }


        $category->update();

        return response()->json([
            'message' => __('messages.category-has-been-updated')
        ], 200);
    }

    public function UpdateStatus(Request $request)
    {
       $data = $request->validate([
            'category_id' => 'required|numeric|exists:categories,id',
            'show' => 'required|bool'
        ]);
        $category = Category::find($data['category_id']);
        $category->update([
            'show' => $data['show']
        ]);

        return $category;
    }

    public function destroy(Category $category)
    {
        $category =  $category->delete();

        return response()->json([
            'message' => __('messages.category-has-been-deleted')
        ], 201);
    }
}
