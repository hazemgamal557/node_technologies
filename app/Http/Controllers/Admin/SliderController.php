<?php

namespace App\Http\Controllers\Admin;
use App\Services\FileManager;
use App\Http\Controllers\Controller;
use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function index(Request $request)
    {
        $record = Slider::orderBy('id', 'desc')
                   ->paginate($request->query('limit', 10));
        return $record;
    }

    public function add(Request $request, FileManager $fileManager){
        $data = $request->validate([
            'image' => 'required|image|mimes:jpeg,jpg,png|max:20000'
        ]);

        $slider = new Slider;
        if ($request->hasFile('image')  && request()->file('image')->isValid()) {
            $slider->image = $fileManager->uploadSliderImage(request()->file('image'));
        }
        $slider->save();
        return response()->json([
            'message' => 'added image suceess'
        ], 201);
    }

    public function destroy(Slider $slider)
    {
        $slider =  $slider->delete();

        return response()->json([
            'message' => 'slider deleted successfully'
        ], 201);
    }
}
