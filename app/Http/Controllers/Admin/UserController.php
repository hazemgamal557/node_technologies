<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('id', 'desc')
        ->where('activated_by_code', 1)
        ->paginate(10);
        return response()->json($users, 200);
    }



    public function indexAll()
    {
        $users = User::orderBy('id', 'desc')
        ->where('activated_by_code', 1)
        ->get();
        return response()->json($users, 200);
    }
    
    
    public function show($id){
        $user = User::findOrFail($id);
        return $user;
    }

    public function block($id)
    {
        $user = User::findOrFail($id);
        $user->blocked = 1;
        $user->update();

        return response()->json([
            'message' => 'User has been blocked',
        ], 201);
    }

    public function unblock($id)
    {
        $user = User::findOrFail($id);
        $user->blocked = 0;
        $user->update();

        return response()->json([
            'message' => 'User has been unblocked',
        ], 201);
    }

}
