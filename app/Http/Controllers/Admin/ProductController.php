<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetails;
use App\Product;
use App\SubCategory;
use App\ProductMedia;
use App\Services\FileManager;
use Illuminate\Database\Eloquent\Collection;
// use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $input = $request->validate([
            'q' => 'string',
            'limit' => 'integer|min:1',
            'subcategory' => 'string',
        ]);
            
        $productsQuery = Product::with([
            'subCategory',
            'media',
        ])
            ->orderBy('id', 'DESC');

        

        if (!empty($input['q'])) {

            $productsQuery->where(function ($query) use ($input) {
                $query->where('name_ar', 'LIKE', "%{$input['q']}%");
            });
            
        }


        if (!empty($input['subcategory'])) {

            $productsQuery->whereHas('subCategory', function ($query) use ($input) {
                $query->where('name_ar', 'LIKE', "%{$input['subcategory']}%");
            });
        }

        // if (!empty($input['subcategory_id'])) {
        //     $subcategory = SubCategory::where('name_ar', $input['subcategory_id'])->first();
        //     $subcategoryID = $subcategory->id;
        //     $productsQuery->where('sub_categories_id', $subcategoryID);
        // }

        return $productsQuery->paginate(
            $request->query('limit', 10)
        );
    }


    /** fetch all products with not pagination */
    public function indexAll(){

        $products = Product::all();
        return $products;
    }


    public function store(Request $request, FileManager $fileManager)
    {
        $data = $request->validate([
            'name_ar' => 'required|string',
            'name_en' => 'required|string',
            'type_ar' => 'string',
            'type_en' => 'string',
            'trademark_ar' => 'string',
            'trademark_en' => 'string',
            'price' => 'required|numeric',
            'description_ar' => 'string',
            'description_en' => 'string',
            'images' => 'required|array|min:1|max:7',
            'images.*' => 'image|mimes:jpeg,jpg,png|max:20000',
            'sub_categories_id' => 'required|integer|exists:sub_categories,id',
        ]);

        $product = Product::create([
            'name_ar' => $data['name_ar'],
            'name_en' => $data['name_en'],
            'type_ar' => $data['type_ar'] ?? null,
            'type_en' => $data['type_en'] ?? null,
            'price' => $data['price'],
            'trademark_ar' => $data['trademark_ar'] ?? null,
            'trademark_en' => $data['trademark_en'] ?? null,
            'description_ar' => $data['description_ar'] ?? null,
            'description_en' => $data['description_en'] ?? null,
            'sub_categories_id' => $data['sub_categories_id'],

        ]);

        $productImagesNames = $fileManager->uploadProductImages(
            $data['images']
        );

        $media = $this->prepareTheProductMediaRecords($productImagesNames);
        $product->media()->createMany($media->toArray());
        $product->load(['media', 'subCategory']);

        return response()->json([
            'record' => $product,
            'message' => 'record has been created successfully',
        ]);
    }

    public function update(Request $request, int $id, FileManager $fileManager)
    {
        $input = $request->validate([
            'name_ar' => 'required|string',
            'name_en' => 'required|string',
            'type_ar' => 'string',
            'type_en' => 'string',
            'trademark_ar' => 'string',
            'trademark_en' => 'string',
            'price' => 'numeric',
            'description_ar' => 'string',
            'description_en' => 'string',
            'images' => 'array|min:1|max:7',
            'images.*' => 'image|mimes:jpeg,jpg,png|max:20000',
            'sub_categories_id' => 'required|integer|exists:sub_categories,id',
        ]);

        $product = Product::with('media')
            ->where('id', $id)
            ->firstOrFail();

        $fields = [
            'name_ar',
            'name_en',
            'type_ar',
            'type_en',
            'price',
            'trademark_ar',
            'trademark_en',
            'description_ar',
            'description_en',
            'sub_categories_id',
        ];

        foreach ($fields as $field) {
            if (!empty($input[$field])) {
                $product->$field = $input[$field];
            }
        }

        $product->save();

        if (!empty($input['images'])) {
            $productImagesNames = $fileManager->uploadProductImages(
                $input['images'],
                $this->getProductImagesNames($product->media)
            );

            $media = $this->prepareTheProductMediaRecords($productImagesNames);
            $product->media()->delete();
            $product->media()->createMany($media->toArray());
        }

        $product->load(['media', 'subCategory']);

        return response()->json([
            'record' => $product,
            'message' => 'record has been updated successfully',
        ]);
    }

    public function show(Request $request, int $productId)
    {
        return Product::with([
            'subCategory',
            'media',
        ])
            ->findOrFail($productId);
    }

    public function attachProductImage(Request $request, int $productId, FileManager $fileManager)
    {
        $input = $request->validate([
            'image' => 'required|image|mimes:jpeg,jpg,png|max:20000',
            'caption' => 'boolean',
        ]);

        $product = Product::where('id', $productId)->firstOrFail();

        if (!empty($input['caption'])) {
            $product->media()->update(['caption' => false]);
        }

        $productImageName = $fileManager->uploadProductImage(
            $input['image']
        );

        $product->media()->create([
            'image' => $productImageName,
            'caption' => $input['caption'] ?? false,
        ]);

        return response()->json([
            'message' => 'the product image has been added successfully',
        ], 201);
    }

    /**
     * remove the given image id
     *
     * @return \Illuminate\Http\Response
     */
    public function removeProductImage(int $imageId, FileManager $fileManager)
    {
        $image = ProductMedia::findOrFail($imageId);
        $fileManager->remove($image->getAttributes()['image']);
        $image->delete();

        return response()->json([
            'message' => 'the product image has been deleted successfully',
        ], 201);
    }

    /**
     * remove the product with its video & images
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function remove(int $id)
    {
        $product = Product::with('orderDetail')
                ->findOrFail($id);
        $product->delete();
    
        $orders = Order::where('status', 'cart')
                ->get();

        /** Delete this product from user cart */
        foreach ($orders as $order) {
            OrderDetails::where('product_id', $id)
                ->where('order_id', $order->id)
                ->delete();
        }

        return response()->json([
            'message' => 'the product has been deleted successfully',
        ], 201);
    }

    private function prepareTheProductMediaRecords(array $productImagesNames)
    {
        $media = collect($productImagesNames)->map(function ($imageName) {
            return [
                'image' => $imageName,
                'caption' => false,
            ];
        });

        $caption = $media->shift();
        $caption['caption'] = true;
        $media->prepend($caption);

        return $media;
    }
    private function getProductImagesNames(Collection $media)
    {
        $imagesNames = [];

        foreach ($media as $image) {
            $imagesNames[] = $image->getAttributes()['image'] ?? '';
        }

        return $imagesNames;
    }
}
