<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function setDeviceId(Request $request)
    {
        $data = $request->validate([
            'device_id' => 'required|string'
        ]);

        User::where('device_id', $data['device_id'])
            ->where('id', '!=', \Auth::id())
            ->update(['device_id' => null]);


        $authUser = \Auth::user();
        $authUser->device_id = $data['device_id'];
        $authUser->save();

        return response()->json([
            "message" => "added-success"
        ], 201);
    }
}
