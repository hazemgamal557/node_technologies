<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderDetailResource;
use App\Order;
use App\OrderDetails;
use App\Product;
use Auth;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function getAllOrders()
    {


        $order = Order::where('status', 'cart')->where('user_id', \Auth::user()->id)->get();

        if (!$order->isEmpty()) {
            $orderDetails = OrderDetails::where('order_id', $order[0]['id'])->get();
            $orders = OrderDetailResource::collection($orderDetails);
            $totalPrice = 0;
            $totalQuantity = 0;
            foreach($orderDetails as $o){
                $totalQuantity += $o->quantity;
                $totalPrice += $o->quantity * $o->product->price;
            }
            return response()->json([

               "orders" => $orders,
               "totalQuantity" => $totalQuantity,
               "totalPrice"   => $totalPrice
                ]);
        } else {
            return response()->json([
                'message' => 'the card is empty',
            ], 200);
        }
    }

    public function getOrderWhereNotCard()
    {
        $order = Order::where('user_id', \Auth::user()->id)->where('status', '!=', 'cart')->get();

        $orders = OrderResource::collection($order);
        if (!$order->isEmpty()) {
            $totalPrice = 0;
            $totalQuantity = 0;
            $orderDetails = OrderDetails::where('order_id', $order[0]['id'])->get();

            foreach ($orderDetails as $o) {
                $totalQuantity += $o->quantity;
                $totalPrice += $o->quantity * $o->product->price;
            }
            $totalPrice = round($totalPrice, 2);
            return response()->json([
                'orders' => $orders,
                'totalQuantity' => $totalQuantity,
                'totalPrice' => $totalPrice,

            ]);
        } else {
            return response()->json([
                'message' => 'no order has ended yet',
            ], 200);
        }

    }

    public function AddToOrderDetail(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $cart = Order::where('user_id', \Auth::user()->id)
            ->where('status', 'cart')
            ->first();

        if (empty($cart)) {
            $cart = new Order;
            $cart->user_id = \Auth::user()->id;
            $cart->status = 'cart';
            $cart->save();
        }
        $orderDetail = OrderDetails::where('product_id', $product->id)->where('order_id', $cart->id)->first();

        if(!is_null($orderDetail))
        {
            return response()->json([
                'message' => 'sorry this order of product is already exist in orders data'
            ], 401);
        }
        $datails = new OrderDetails;
        $datails->order_id = $cart->id;
        $datails->product_id = $product->id;
        $datails->quantity = 1;
        $datails->save();

        return response()->json([
            'message' => 'your order has been addedd successfully added',
        ], 201);

    }

    public function updateOrderDetail(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        if ($order->user_id != \Auth::user()->id) {
            return response()->json([
                'message' => __('message.sorry you are not authorized to updated this orderes'),
            ], 401);
        }
        $order->status = 'ordered';
        $order->save();

        $orders = OrderDetails::where('order_id', $order->id)->get();
        foreach ($orders as $order) {

            $order->price = $order->product->price;
            $order->update();
        }

        return response()->json([
            'message' => __('message.order send successfully'),
        ], 201);
    }

    public function updateOrderDetailQuantity(Request $request, $id)
    {
        $data = $request->validate([
            'quantity' => 'required|integer|min:1',
        ]);
        $order_detail = OrderDetails::findOrFail($id);

        $order_user = $order_detail->order->user_id;
        if ($order_user == \Auth::user()->id) {
            $order_detail->quantity = $data['quantity'];
            $order_detail->update();
            return response()->json([
                'message' => __('message.your quantity updated successfully'),
            ], 201);
        } else {
            return response()->json([
                'message' => __('message.sorry you are not authorized to updated this orderes'),
            ], 401);
        }
    }

    public function deleteOrderDetail(Request $request, $id)
    {
        $order_detail = OrderDetails::findOrFail($id);

        $user_order = $order_detail->order->user_id;
        if ($user_order == \Auth::user()->id) {
            $order_detail->delete();
            return response()->json([
                'message' => __('message.your order is deleted'),
            ], 201);
        } else {
            return response()->json([
                'message' => 'unauthourized to delete this order',
            ], 401);
        }

    }
}
