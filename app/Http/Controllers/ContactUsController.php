<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactUs;

class ContactUsController extends Controller
{
    public function store(Request $request)
    {
        $input = $request->validate([
            'name'    => 'required|string|max:30',
            'email'   => 'required|string|max:30|email',
            'message' => 'required|string'
        ]);
        ContactUs::create($input);

        return response()->json([
            'message' => 'تم ارسال الرسالة بنجاح'
        ], 201);
    }}
