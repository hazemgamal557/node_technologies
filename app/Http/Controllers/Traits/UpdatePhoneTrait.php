<?php

namespace App\Http\Controllers\Traits;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Services\FileManager;
use Illuminate\Support\Facades\Hash;
use Auth;
trait UpdatePhoneTrait
{
    /**
     * send the given sms $message to the given $phone
     *
     * @return void
     */
    // abstract protected function sendSMS(string $phone, string $message) : void;

    /**
     * generate the confirm_code field
     *
     * @return string
     */
    abstract protected function generateConfirmationCode() : string;

    /**
     * send the confirmation code to the auth user new phone
     * here we update: [confirm_code, request_confirm_code_date, requested_new_phone]
     *
     * @param App\Http\Requests\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendUpdatePhoneConfirmationCode(Request $request)
    {
        $phone = $request->validate([
            'phone' => [
                'required',
                'string',
                Rule::unique('users')->where('activated_by_code', true)->ignore(\Auth::id())
            ]
        ])['phone'];
        // $phone = '00966' . $phone;

        // return response($phone);
        $confirmCode = $this->generateConfirmationCode();

        $user = \Auth::user();
        $user->confirm_code = $confirmCode;
        $user->request_confirm_code_date = now();
        $user->request_new_phone = $phone;
        $user->save();

        // $this->sendSMS($phone, $confirmCode);

        return response()->json([
            'ttl'     => User::CONFIRM_CODE_EXPIRES_IN_SEC,
            'message' =>  __('message.confirm-code-sent')
        ], 201);
    }
    // update resend confirmation code
    public function resendUpdateConfirmationCode(Request $request)
    {
        $data = $request->validate([
            'phone' => [
                'required',
                'string',
                Rule::unique('users')->where('activated_by_code', true)->ignore(\Auth::id())
            ]
        ]);
        $phone = $data['phone'];
        $confirmCode = $this->generateConfirmationCode();
        $user = User::where('phone', \Auth::user()->phone)->where('request_new_phone', '!=', null)->first();
            if(is_null($user))
            {
                return response()->json([
                    'message' => 'sorry you are not avaliable phone'
                ], 401);
            }
        $confirmCode = $this->generateConfirmationCode();
        $user->confirm_code = $confirmCode;
        $user->request_confirm_code_date = now();
        $user->request_new_phone = $phone;
        $user->save();

        return response()->json([
            'ttl'     => User::CONFIRM_CODE_EXPIRES_IN_SEC,
            'message' =>  __('message.confirm-code-sent')
        ], 201);
    }
    // update name , address and image
    public function updateNameAddressAndImage(Request $request, FileManager $fileManager)
    {
         $request->validate([
            'name'    => 'string|nullable',
            'address' => 'string|nullable',
            'image'   => 'image|mimes:jpeg,jpg,png|max:20000|nullable'
        ]);

        $user = User::where('phone', \Auth::user()->phone)
        ->where('activated_by_code', true)->first();
        if(! $user)
        {
            return response()->json([
                'message' => __('message.no active user has this phone number')
            ], 401);
        }

        // $user->address = $request->address;
        if(!empty($request->name))
        {
            $user->name = $request->name;
        }
        if(!empty($request->address))
        {
            $user->address = $request->address;
        }

        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $user->image = $fileManager->uploadUserImage(request()->file('image'));
        }

        $user->save();
        return response()->json([
            'message' =>  __('message.updated-user-data-sucessfully')
        ], 201);
    }


    /**
     * check the given confirmation code with the stored one
     * here we update: [request_update_phone_date, phone]
     *
     * @param App\Http\Requests\Request $request
     * @return \Illuminate\Http\Response
     */
    public function checkConfirmationCodeAndUpdatePhone(Request $request)
    {
        $data = $request->validate(['confirm_code' => 'required']);

        $user = \Auth::user();

        if (!$user->isConfirmCodeActive()) {
            return response()->json([
                'message' => __('message.this code is expired please try again')
            ], 410);
        }


        if ($user->confirm_code != $data['confirm_code']) {
            $user->confirm_code = null;
            $user->save();

            return response()->json([
                'message' => __('message.this confirm code is not right please try agian')
            ], 401);
        }

        $user->confirm_code = null;
        $user->request_confirm_code_date = null;
        $user->phone = $user->request_new_phone;
        $user->request_new_phone = null;
        $user->save();

        return response()->json([
            'message' => 'updated-sucessfully'
        ], 201);
    }
}
