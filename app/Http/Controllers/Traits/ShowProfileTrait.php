<?php

namespace App\Http\Controllers\Traits;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Resources\UserResource;
use App\Services\FileManager;
use Illuminate\Support\Facades\Hash;
use Auth;

trait ShowProfileTrait
{
    public function ShowProfile(Request $request)
    {
        $user = User::where('id', \Auth::user()->id)->first();
        return new UserResource($user);
    }
}
