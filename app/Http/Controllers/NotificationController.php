<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use App\Notification;
use App\User;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * send notification to all|specific user
     *
     * @param int $_GET['page']
     * @param int $_GET['limit']
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        $notifications = Notification::orderBy('id', 'desc')
            ->where('user_id', \Auth::id())
            ->paginate(
                $request->query('limit', 10)
            );

        $notificationCount = $notifications->filter(function ($count) {
            return $count['seen'] == false;
        })->count();

        $notification = NotificationResource::collection($notifications);

        return response()->json([
            'unread_count' => $notificationCount,
            'notifications' => $notification,
            ]);
    }

    public function markAsRead()
    {

        $notifications = Notification::where('user_id', \Auth::id())
            ->where('seen', false);

        $notifications->update(['seen' => true]);

        return response()->json([
            'message' => __('message.the-given-notifications-have-been-marked-as-read-successfully'),
        ]);
    }

}
