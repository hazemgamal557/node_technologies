<?php

namespace App\Http\Controllers;
use App\Http\Resources\CategoryResource;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index(Request $request)
    {
        $recordes = Category::orderBy('id', 'desc')->where('show', true)
        ->paginate($request->query('limit', 10));

        $categories = CategoryResource::collection($recordes);
        return response()->json($categories);
    }

    public function show($id)
    {
        $category = Category::findOrFail($id);
         $record = new CategoryResource($category);
         return response()->json($record);
    }
}
