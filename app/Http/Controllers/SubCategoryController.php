<?php

namespace App\Http\Controllers;
use App\Http\Resources\SubCategoryResource;
use App\Http\Resources\SpicialSupCategoryResource;
use App\SubCategory;
use App\Category;
use App\Product;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{

    public function index(Request $request)
    {
        $input = $request->validate([
            'category_id' => 'numeric|exists:categories,id'
        ]);

        $subCategoryQuery = SubCategory::with(['category'])->orderBy('id', 'DESC');

        // return $subCategoryQuery;
        if(!empty($input['category_id'])){
            $subCategoryQuery->where(function($query) use ($input){
                $query->where('category_id', $input['category_id'])->where('show', true);
            });
        }

        // return SubCategoryResource::collection($subCategoryQuery);
        $myRecord = $subCategoryQuery->paginate($request->query('limit', 10));

        $subcats =  SubCategoryResource::collection($myRecord);
        return response()->json($subcats);
    }
}
