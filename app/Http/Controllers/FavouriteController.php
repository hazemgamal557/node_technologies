<?php

namespace App\Http\Controllers;

use App\Favourite;
use App\Product;
use App\Http\Resources\FavouriteResource;
use Illuminate\Http\Request;
use Auth;
class FavouriteController extends Controller
{
    public function add($id)
    {
        $product = Product::findOrFail($id);

        $check = Favourite::where([
            ['user_id', '=', \Auth::user()->id],
            ['product_id', '=', $id]
        ])->first();

        if(! empty($check))
        {
            $check->delete();
            return response()->json([
                'message' => 'this favourite has been deleted'
            ]);
        }

        $favourite = new Favourite;
        $favourite->user_id = \Auth::user()->id;
        $favourite->product_id = $id;
        $favourite->save();

        return response()->json([
            'message' => 'the favourite has been added successfuly'
        ]);

    }

    public function show()
    {
        $recordes = Favourite::where('user_id', '=', Auth::user()->id)->get();
        $favourites = FavouriteResource::collection($recordes);
        return response()->json($favourites);
    }
}
