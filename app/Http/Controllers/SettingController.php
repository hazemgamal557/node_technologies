<?php

namespace App\Http\Controllers;

use App\Setting;

class SettingController extends Controller
{
    public function retriveSocialMedia(Setting $setting)
    {
        $settings = Setting::where('key', 'LIKE', 'social_%')->get();

        // return response()->json($settings);
        return response()->json([
            'facebook'  => $settings->where('key', 'social_facebook')->first()->content,
            'twitter'   => $settings->where('key', 'social_twitter')->first()->content,
            'instagram' => $settings->where('key', 'social_instagram')->first()->content,
            'youtube'   => $settings->where('key', 'social_youtube')->first()->content,
            'whatsapp'  => $settings->where('key', 'social_whatsapp')->first()->content,
            'phone'     => $settings->where('key', 'social_phone')->first()->content,
            'gmail'     => $settings->where('key', 'social_gmail')->first()->content
        ]);
    }
    public function getPage(string $key)
    {
        // dd(app()->getLocale());
        $key = "page_{$key}_" . app()->getLocale();

        $page = Setting::where('key', $key)
            ->firstOrFail()
            ->content;

        return response()->json([
            'content' => $page,
        ]);
    }
}
