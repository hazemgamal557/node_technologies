<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\UpdatePhoneTrait;
use App\Http\Controllers\Traits\ShowProfileTrait;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Services\FileManager;

class AuthController extends Controller
{
    use UpdatePhoneTrait;
    use ShowProfileTrait;
    public function registerAndSendConfirmationCode(Request $request, FileManager $fileManager)
    {
        $data = $request->validate([
            'phone' => [
                'required',
                'string',
                Rule::unique('users')->where('activated_by_code', true),
            ],
            'name' => 'required|string',
            'address' => 'string',
        ]);


        User::where('phone', $data['phone'])
            ->where('activated_by_code', false)
            ->delete();

        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $data['image'] = $fileManager->uploadUserImage(request()->file('image'));
        }

        $user = User::create($data);

        // send confirmation code
        $confirmCode = $this->generateConfirmationCode();
        $this->sendSMS($data['phone'], $confirmCode);

        $user->confirm_code = $confirmCode;
        $user->activated_by_code = false;
        $user->request_confirm_code_date = now();
        $user->save();

        return response()->json([
            'ttl' => User::CONFIRM_CODE_EXPIRES_IN_SEC,
            'message' => __('message.confirm-code-sent'),
        ], 201);
    }



    public function activateRegisteredUser(Request $request)
    {
        $data = $request->validate([
            'phone' => 'required|string',
            'confirm_code' => 'required|string',
        ]);
        $user = User::where('phone', $data['phone'])->where('activated_by_code', false)->first();
        if (!$user) {
            return response()->json([
                'message' => __('message.no active user has this phone number'),
            ], 401);
        }
        if (!$user->isConfirmCodeActive()) {
            return response()->json([
                'message' => __('message.this code is expired please try again'),
            ], 401);
        }
        if($user->confirm_code != $data['confirm_code'])
        {
            $user->save();
            return response()->json([
                'message' => __('message.this confirm code is not right please try agian'),
            ], 401);
        }
        $user->confirm_code = null;
        $user->activated_by_code = true;
        $user->request_confirm_code_date = null;
        $user->save();
        return response()->json([
            'user' => new UserResource($user),
            'token' => $user->createToken('user_token', ['server:show'])->plainTextToken,
            'message' => __('message.activated code successfully'),
        ], 201);
    }



    public function resendConfirmationCode(Request $request)
    {
        $data = $request->validate([
            'phone' => 'required|string',
        ]);
        $phone = $data['phone'];
        $confirmCode = $this->generateConfirmationCode();
        $this->sendSMS($data['phone'], $confirmCode);

        $user = User::where('phone', $data['phone'])
            ->where('activated_by_code', false)
            ->first();
        if (!$user) {
            return response()->json([
                'message' => __('message.no active user has this phone number'),
            ], 401);
        }
        $user->confirm_code = $confirmCode;
        $user->activated_by_code = false;
        $user->request_confirm_code_date = now();
        $user->save();
        return response()->json([
            'ttl' => User::CONFIRM_CODE_EXPIRES_IN_SEC,
            'message' => __('message.confirm-code-sent'),
        ], 201);
    }
    public function requestLogin(Request $request)
    {
        $data = $request->validate([
            'phone' => 'required|string',
        ]);
        $phone = substr($data['phone'], 1, strlen($data['phone']));
        $confirmCode = $this->generateConfirmationCode();
        $this->sendSMS($phone, $confirmCode);

        $user = User::where('phone', $data['phone'])
        ->where('activated_by_code', true)
        ->where('blocked', false)
        ->first();
        if (is_null($user)) {
            if (!$user) {
                return response()->json([
                    'message' => __('message.no active user has this phone number'),
                ], 401);
            }
        }
        $user->confirm_code = $confirmCode;
        $user->request_confirm_code_date = now();
        $user->save();

        return response()->json([
            'ttl' => User::CONFIRM_CODE_EXPIRES_IN_SEC,
            'message' => __('message.confirm-code-sent'),
        ], 201);
    }
    public function login(Request $request)
    {
        $data = $request->validate([
            'phone' => 'required|string',
            'confirm_code' => 'required|string',
        ]);
        $user = User::where('phone', $data['phone'])->where('activated_by_code', true)->first();

        if (is_null($user)) {
            if (!$user) {
                return response()->json([
                    'message' => __('message.no active user has this phone number'),
                ], 401);
            }
        }

        if (!$user->isConfirmCodeActive()) {
            return response()->json([
                'message' => __('message.this code is expired please try again'),
            ], 401);
        }
        if ($user->confirm_code != $data['confirm_code']) {
            $user->confirm_code = null;
            $user->save();
            return response()->json([
                'message' => __('message.this confirm code is not right please try agian'),
            ], 401);
        }
        $user->confirm_code = null;
        $user->activated_by_code = true;
        $user->request_confirm_code_date = null;
        $user->save();

        return response()->json([
            'user' => new UserResource($user),
            'token' => $user->createToken('user-token', ['server:show'])->plainTextToken,
            'message' => __('message.loggin successfully'),
        ], 201);
    }
    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json([
            'message' => __('message.logged out'),
        ], 200);
    }
    protected function generateConfirmationCode(): string
    {
       //  return (string) 1111;
        return (string) rand(1000, 9999);

    }



    /**
     * to send sms we are using qyadat
     * check the following link:
     *      http://www.qyadat.com/sms
     *
     * @return void
     */
    public function sendSMS(string $phone, string $otp)
    {
        $user = env('QADAT_USER');
        $sender = env('QADAT_SENDER');
        $password = env('QADAT_PASSWORD');

        $message = "Verification%20code%20is:{$otp}";
        //$queryString = "username={$user}&password={$password}&numbers={$phone}&sender={$sender}&unicode=U&return=Json";
        $queryString = "http://www.qyadat.com/sms/api/sendsms.php?username=966566843000&password=225588&message=". $message ."&numbers=". $phone ."&sender=ELJEELSTORE&unicode=u&return=Json";
        $connection = curl_init();
        curl_setopt($connection, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; U; Linux i686; pt-BR; rv:1.9.2.18) Gecko/20110628 Ubuntu/10.04 (lucid) Firefox/3.6.18');
        curl_setopt($connection, CURLOPT_URL, $queryString);
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);



        try {
            $response = curl_exec($connection);
            curl_close($connection);

            \Log::info('success: sms response', [
                'response' => $response
            ]);
        }
        catch (\Exception $e) {
            \Log::info('fail: sms response', [
                'response_error' => $e->getMessage()
            ]);
        }
    }

    public function setLang(Request $request)
    {
        $data = $request->validate([
            'lang' => ['required', 'regex:/^(ar|en)$/'],
        ]);

        $authUser = \Auth::user();

        // $authUser->update([
        //     'lang' => $data['lang']
        // ]);

        $authUser->lang = $data['lang'];
        $authUser->save();

        return response()->json([
            'message' => __('message.updated-language-sucessfully')
        ], 201);
    }
}
