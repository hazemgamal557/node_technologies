<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'seen' => (bool) $this->seen,
            'message' => $this->{'message_' . app()->getLocale()},
            'created_at' => $this->created_at->diffForHumans(null, true),
        ];
    }
}
