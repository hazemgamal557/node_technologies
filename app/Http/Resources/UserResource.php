<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
       $users = [
           'id' => $this->id,
           'name' => $this->name,
           'phone' => $this->phone,
           'lang'  => $this->lang,
           'address' => $this->address == null ? 'no address yet': $this->address,
           'image' => $this->image
       ];

       return $users;
    }
}
