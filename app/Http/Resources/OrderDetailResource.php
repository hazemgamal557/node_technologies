<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'ordered_product_id' => $this->id,
            'quantity' => $this->quantity,
            'order_id' => $this->order_id,
            'product' => new ProductResource($this->product)
        ];
    }
}
