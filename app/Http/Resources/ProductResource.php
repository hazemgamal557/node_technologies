<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $product = [
            'id' => $this->id,
            'name' => $this->{'name_'.app()->getLocale()},
            'trademark' => $this->{'trademark_'.app()->getLocale()},
            'type' => $this->{'type_'.app()->getLocale()},
            'description' => $this->{'description_'.app()->getLocale()},
            'price' => $this->price,
            'is_favourite' => (bool) $this->is_favourite == true ? true : false ,
            'is_card' => (bool) $this->is_card == true ? true : false ,
            'subcategory_id' => $this->sub_categories_id,
            'product_media' => ProductMediaResource::collection($this->media)
        ];
        return $product;
    }
}
