<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $orders = [
            // 'id' => $this->id,
            // 'user_id' => $this->user_id,
            'orderDetails' => OrderDetailResource::collection($this->orderDetail)
        ];

        // if(app()->getLocale() == 'ar'){
        //     if($this->status == 'cart'){
        //         $orders['status'] = 'سلة';
        //     }elseif($this->status == 'ordered'){
        //         $orders['status'] = 'مطلوب';
        //     }elseif($this->status == 'delivered'){
        //         $orders['status'] = 'انتهت';
        //     }
        // }else{
        //     $orders['status'] = $this->status;
        // }
        return $orders;
    }
}
