<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FavouriteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->product->is_favourite = true;
        // return parent::toArray($request);
        return [
            // 'id' => $this->id,
            'product' => new ProductResource($this->product)
        ];
    }
}
