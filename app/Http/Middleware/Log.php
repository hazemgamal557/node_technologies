<?php

namespace App\Http\Middleware;

use Closure;

class Log
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $requestData = $request->all();
        if (isset($requestData['password'])) {
            unset($requestData['password']);
        }

        try {
            \Log::info($request->method() . ' ⇒ ' . $request->getPathInfo() . ' ⇒ ' . $response->status(), [
                'request' => $requestData,
                'response' => $response instanceof \Illuminate\Http\JsonResponse
                    ? $response->getOriginalContent()
                    : 'Not A JSON Response'
            ]);
        }
        catch(\Exception $e) {}

        return $response;
    }
}
