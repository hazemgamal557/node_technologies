<?php

namespace App\Http\Middleware;

use Closure;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $language = $request->headers->get('Accept-Language');
        if($language)
        {
            app()->setLocale($language);
        }
        $authUser = auth()->user();
        if($authUser && $authUser->lang)
        {
            app()->setLocale($authUser->lang);
        }
        return $next($request);
    }
}
