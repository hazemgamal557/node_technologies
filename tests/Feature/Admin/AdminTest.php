<?php

namespace Tests\Feature\Admin;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
 
    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

    
    }


    public function test_store_admins()
    {
  
        // arrange
        $admin = [
            'name'  => 'اسم',
            'email'  => 'email@email.com',
            'password'  => 'eweqeqweqwewe',
        ];

        // act
        $response = $this->actingAs($this->admin)
            ->json('POST', '/api/admin/', $admin);
        // dd($response);
        // assert
        $response->assertStatus(201);
        
        $record = json_decode($response->getContent(), true);

        # assert record created
        $this->assertDatabaseHas('admins', [
            'name' => $admin['name'],
        ]);

    }


    // public function test_remove_admins()
    // {
    //     // arrange
    //     $admin = factory('App\Admin')->create();

        
    //     // act
    //     $response = $this->actingAs($this->admin)
    //         ->json('DELETE', "/api/admin/{$admin->id}");
    //     // assert
    //     $response->assertStatus(201);

    //     $this->assertDatabaseMissing('admins', [
    //         'id'   => $admin->id,
    //         'name' => $admin->name,
    //     ]);
    // }


    public function test_retrieve_admins(){

        factory('App\Admin', 3)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/admin/');

            // dd(json_decode($response->getContent()));
                 // assert
        $response->assertStatus(200);

        // $this->assertCount(2, json_decode($response->getContent()));
        $this->assertCount(4, $response->json()['data']);

    }

    
            //     dd($record);
            // # image exist
            // $image = explode('storage', $record['image'])[1];
            // Storage::assertExists("public{$image}");
    
            // # remove test files
            // Storage::delete("public/{$image}");
        

        public function test_update_admins()
            {
                // arrange
                $admin = factory('App\Admin')->create();
        
                $form = [
                    'name'  => 'اسم',
                ];
        
                // act
                $response = $this->actingAs($this->admin)
                    ->json('POST', "/api/admin/update-my-profile/", $form);
        
                // assert
                $response->assertStatus(201);
        
                # assert record created
                $this->assertDatabaseHas('admins', [
                    'name'  => 'اسم',
                ]);
            }

         
}

