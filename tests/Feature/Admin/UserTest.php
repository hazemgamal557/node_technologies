<?php

namespace Tests\Feature\Admin;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    private $user;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        $this->user = \App\User::create([
            'name'     => 'أدمن',
            'phone'     => '156648851',

        ]);

    }

    public function test_retrieve_users(){

        factory('App\User', 3)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/admin/users');

                 // assert
        $response->assertStatus(200);
        // dd($response->json());
        $this->assertCount(3, $response->json()['data']);

    }





        public function test_update_users()
            {
                // arrange
                $user = factory('App\User')->create();


                // act
                $response = $this->actingAs($this->user)
                    ->json('POST', "/api/admin/users/{$user->id}/block");

                // assert
                $response->assertStatus(201);

                # assert record created
                $this->assertDatabaseHas('users', [
                    'blocked'  => 0,
                ]);
            }


}


