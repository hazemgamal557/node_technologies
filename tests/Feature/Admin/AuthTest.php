<?php

namespace Tests\Feature\Admin;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
 
    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

    
    }


    public function test_login()
    {
        // arrange
        $auth = [
            'email'  => 'admin@admin.com',
            'password'  => 'secret',
        ];

        // act
        $response = $this->json('POST', '/api/auth/superadmin/login', $auth);

        // assert
        $response->assertStatus(200);

        $record = json_decode($response->getContent(), true);

        # assert record created
        $this->assertDatabaseHas('admins', [
            'email' => $auth['email'],

        ]);
    }

         
    public function test_logout()
    {
        // arrange
        $auth = [
            'email'  => 'admin@admin.com',
            'password'  => 'secret',
        ];

        // act
        $response = $this->actingAs($this->admin)
        ->json('POST', '/api/auth/superadmin/logout');

        // assert
        $response->assertStatus(200);

        $record = json_decode($response->getContent(), true);

      
    }

}

