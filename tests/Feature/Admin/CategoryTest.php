<?php

namespace Tests\Feature\Admin;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
 
    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

    
    }

    public function test_retrieve_categories(){

        factory('App\Category', 3)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/admin/category?limit=2');

                 // assert
        $response->assertStatus(200);

        $this->assertCount(2, $response->json()['data']);

    }

    public function test_store_categories()
        {
            // arrange
            $category = [
                'name_ar'  => 'اسم',
                'name_en'  => 'اسم',
                'image' => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
            ];
    
            // act
            $response = $this->actingAs($this->admin)
                ->json('POST', '/api/admin/category/add', $category);
            // assert
            $response->assertStatus(201);
            
            $record = json_decode($response->getContent(), true);
    
            # assert record created
            $this->assertDatabaseHas('categories', [
                'name_ar' => $category['name_ar'],
            ]);

        } 

        public function test_update_categories()
            {
                // arrange
                $category = factory('App\Category')->create();
        
                $form = [
                    'name_ar'  => 'اسم',
                    'name_en'  => 'name',
                    'image' => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
                ];
        
                // act
                $response = $this->actingAs($this->admin)
                    ->json('POST', "/api/admin/category/{$category->id}/update", $form);
        
                // assert
                $response->assertStatus(200);
        
                # assert record created
                $this->assertDatabaseHas('categories', [
                    'name_ar'  => 'اسم',
                ]);
            }

            public function test_remove_category()
                {
                    // arrange
                    $category = factory('App\Category')->create();
            
                    
                    // act
                    $response = $this->actingAs($this->admin)
                        ->json('DELETE', "/api/admin/category/{$category->id}/delete");
            
                    // assert
                    $response->assertStatus(201);
            
                    $this->assertDatabaseMissing('categories', [
                        'id'   => $category->id,
                        'name_ar' => $category->name_ar,
                    ]);
                }
}

