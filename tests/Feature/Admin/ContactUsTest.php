<?php

namespace Tests\Feature\Admin;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ContactUsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name' => 'أدمن',
            'email' => 'admin@admin.com',
            'password' => \Hash::make('secret'),
        ]);

    }

    public function test_retrieve_messages()
    {

        factory('App\ContactUs', 3)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/admin/contact-us?limit=2');

        // assert
        $response->assertStatus(200);

        $this->assertCount(2, $response->json()['data']);

    }

    public function test_markAsSeen()
    {

        $message = factory('App\ContactUs', 3)->create();
        $response = $this->actingAs($this->admin)
            ->json('POST', "/api/admin/contact-us/{$message[1]->id}/mark-as-seen");

        // assert
        $response->assertStatus(201);

        // $this->assertCount(2, $response->json()['data']);

    }

    public function test_remove_message()
    {
        // arrange
        $message = factory('App\ContactUs', 3)->create();

        // act
        $response = $this->actingAs($this->admin)
            ->json('DELETE', "/api/admin/contact-us/{$message[1]->id}");

        // assert
        $response->assertStatus(201);

        $this->assertDatabaseMissing('categories', [
            'id' => $message[1]->id,
        ]);
    }
}
