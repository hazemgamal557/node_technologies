<?php

namespace Tests\Feature\Admin;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name' => 'أدمن',
            'email' => 'admin@admin.com',
            'password' => \Hash::make('secret'),
        ]);

    }

    public function test_retrieve_products()
    {
        factory('App\Category', 3)->create();
        factory('App\SubCategory', 3)->create();
        factory('App\Product', 3)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/admin/products?limit=2');

        // assert
        $response->assertStatus(200);

        $this->assertCount(2, $response->json()['data']);

    }

    public function test_store_products()
    {

        factory('App\Category', 3)->create();
        $subCat = factory('App\SubCategory', 3)->create();
        // arrange
        $product = [
            'name_ar' => 'اسم',
            'name_en' => 'name',
            'price' => 100,
            'images' => array(UploadedFile::fake()->image('0.jpg', 700, 700)->size(300)),

           'sub_categories_id' => $subCat[1]->id,
        ];

        // act
        $response = $this->actingAs($this->admin)
            ->json('POST', '/api/admin/products/', $product);
        // assert

        $response->assertStatus(200);

        $record = json_decode($response->getContent(), true);

        # assert record created
        $this->assertDatabaseHas('products', [
            'name_ar' => $product['name_ar'],
        ]);

    }

    public function test_update_products()
    {

        // arrange
        factory('App\Category', 3)->create();
        $subCat = factory('App\SubCategory', 3)->create();
        $product = factory('App\Product')->create();

        $form = [
            'name_ar' => 'اسم',
            'name_en' => 'name',
            'sub_categories_id' => $subCat[1]->id,

        ];

        // act
        $response = $this->actingAs($this->admin)
            ->json('POST', "/api/admin/products/{$product->id}", $form);

        // assert
        $response->assertStatus(200);

        # assert record created
        $this->assertDatabaseHas('products', [
            'name_ar' => 'اسم',
        ]);
    }

    public function test_remove_product()
    {
        // arrange
        factory('App\Category', 3)->create();
        factory('App\SubCategory', 3)->create();
        $product = factory('App\Product')->create();

        // act
        $response = $this->actingAs($this->admin)
            ->json('DELETE', "/api/admin/products/{$product->id}");

        // assert
        $response->assertStatus(201);

    }
}
