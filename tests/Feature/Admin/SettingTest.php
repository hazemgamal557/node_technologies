<?php

namespace Tests\Feature\Admin;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Seeder;
use SettingSeeder;


class SettingTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        // $this->seed();
        $this->seed(SettingSeeder::class);

    }

    public function test_retrieve_socialMedia(){


        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/admin/socialLinks');

                 // assert
        $response->assertStatus(200);

        $this->assertCount(8, $response->json());

    }


        // public function test_update_setting()
        //     {

        //         $form = [
        //             'content'  => 'content',
        //         ];

        //         // act
        //         $response = $this->actingAs($this->admin)
        //             ->json('POST', "api/admin/settings/3/update", $form);

        //         // assert
        //         dd($response);
        //         $response->assertStatus(200);

        //         # assert record created
        //         $this->assertDatabaseHas('settings', [
        //             'content'  => 'content',
        //         ]);
        //     }


}

