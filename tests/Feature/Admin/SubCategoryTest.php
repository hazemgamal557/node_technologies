<?php

namespace Tests\Feature\Admin;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class SubCategoryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name' => 'أدمن',
            'email' => 'admin@admin.com',
            'password' => \Hash::make('secret'),
        ]);

    }

    public function test_retrieve_subcategories()
    {

       $cat = factory('App\Category', 3)->create();
        factory('App\SubCategory', 3)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', "/api/admin/subcategory/{$cat[1]->id}/?limit=2");

        // dd(json_decode($response->getContent()));
        // assert
        $response->assertStatus(200);

        // $this->assertCount(2, json_decode($response->getContent()));
        // $this->assertCount(2, $response->json()['data']);

    }

    public function test_store_subcategories()
    {
       $cat = factory('App\Category', 3)->create();

        // arrange
        $category = [
            'name_ar' => 'اسم',
            'name_en' => 'اسم',
            'category_id' =>$cat[1]->id,
        ];

        // act
        $response = $this->actingAs($this->admin)
            ->json('POST', '/api/admin/subcategory/add', $category);
        // dd($response);
        // assert

        $response->assertStatus(201);

        $record = json_decode($response->getContent(), true);

        # assert record created
        $this->assertDatabaseHas('sub_categories', [
            'name_ar' => $category['name_ar'],
        ]);
        //     dd($record);
        // # image exist
        // $image = explode('storage', $record['image'])[1];
        // Storage::assertExists("public{$image}");

        // # remove test files
        // Storage::delete("public/{$image}");
    }

    public function test_update_subcategories()
    {
        $cat = factory('App\Category')->create();

        // arrange
        $category = factory('App\SubCategory')->create();

        $form = [
            'name_ar' => 'اسم',
            'name_en' => 'name',
            'category_id'=>$cat->id,
        ];

        // act
        $response = $this->actingAs($this->admin)
            ->json('POST', "/api/admin/subcategory/{$category->id}/update", $form);
        // assert
        $response->assertStatus(200);

        # assert record created
        $this->assertDatabaseHas('sub_categories', [
            'name_ar' => 'اسم',
        ]);
    }

    public function test_remove_subcategory()
    {
        // arrange
        $category = factory('App\Category')->create();
        $category = factory('App\SubCategory')->create();

        // act
        $response = $this->actingAs($this->admin)
            ->json('DELETE', "/api/admin/subcategory/{$category->id}/delete");

        // assert
        $response->assertStatus(201);

        $this->assertDatabaseMissing('sub_categories', [
            'id' => $category->id,
            'name_ar' => $category->name_ar,
        ]);
    }
}
