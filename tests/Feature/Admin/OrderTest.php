<?php

namespace Tests\Feature\Admin;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    private $admin;

    /**
     * create admin to perform our tests
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name' => 'أدمن',
            'email' => 'admin@admin.com',
            'password' => \Hash::make('secret'),
        ]);

    }

    public function test_retrieve_orders()
    {
        factory('App\User', 3)->create();
        factory('App\Order', 3)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', '/api/admin/order?limit=2');
        // dd(json_decode($response->getContent()));
        // assert
        $response->assertStatus(200);

        // $this->assertCount(2, json_decode($response->getContent()));
        $this->assertCount(2, $response->json()['data']);

    }

    public function test_retrieve_orderDetails()
    {

        factory('App\Category', 3)->create();
        factory('App\SubCategory', 3)->create();
        factory('App\User', 3)->create();
        factory('App\Product', 3)->create();

        $detail =   factory('App\Order', 3)->create();

        factory('App\OrderDetails', 6)->create();

        $response = $this->actingAs($this->admin)
            ->json('GET', "/api/admin/order/{$detail[1]->id}");

        // assert
        $response->assertStatus(200);

        // $this->assertCount(2, $response->json()['data']);

    }

    public function test_update_order()
    {
        factory('App\User', 3)->create();
        // arrange
        $order = factory('App\Order')->create();

        // act
        $response = $this->actingAs($this->admin)
            ->json('POST', "/api/admin/order/{$order->id}/activate");

        // assert
        $response->assertStatus(200);

        # assert record created
        $this->assertDatabaseHas('orders', [
            'status' => "ordered",
        ]);
    }

}
