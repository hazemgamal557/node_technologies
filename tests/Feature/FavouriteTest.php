<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FavouriteTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    private $user;
    public function setUp(): void
    {
        parent::setUp();

        $this->user = \App\User::create([
            'name' => 'hazem gamal',
            'phone' => '01110082125'
        ]);
    }

    public function test_retriveFavourite()
    {
        factory('App\Category', 3)->create();
        factory('App\SubCategory', 3)->create();
        factory('App\Product', 3)->create();
        factory('App\Favourite', 3)->create();

        $response = $this->actingAs($this->user)
                  ->json('GET', '/api/user/favourite/all');

        $response->assertStatus(200);

        $this->assertCount(3, json_decode($response->getContent()));
    }

    public function test_addFavourite()
    {
        factory('App\Category')->create();
        factory('App\SubCategory')->create();
        $product = factory('App\Product')->create();

        $favourite = [
            'user_id' => $this->user->id,
        ];

        $response = $this->actingAs($this->user)
                   ->json('POST', "/api/user/favourite/add/{$product->id}", $favourite);

        // assert
        $response->assertStatus(200);

        $record = json_decode($response->getContent(), true);

        $this->assertDatabaseHas('favourites', [
            'user_id' => $favourite['user_id'],
        ]);
    }

}
