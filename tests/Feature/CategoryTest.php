<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    private $user;
    private $admin;
    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        $this->user = \App\User::create([
            'name' => 'hazem gamal',
            'phone' => '01110082125'
        ]);
    }

    public function test_retrive_categories()
    {
        factory('App\Category', 3)->create();
        $response = $this->actingAs($this->user)
                    ->json('GET', '/api/user/all/categories?limit=2');
        $response->assertStatus(200);
        $this->assertCount(2, json_decode($response->getContent()));
    }

    public function test_retrive_single_category()
    {
        $category = factory('App\Category')->create();
        $response = $this->actingAs($this->user)
                    ->json('GET', "/api/user/single/category/{$category->id}");

        $response->assertStatus(200);
        $record = json_decode($response->getContent(), true);

    }
}
