<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    private $user;
    public function setUp(): void
    {
        parent::setUp();

        $this->user = \App\User::create([
            'name' => 'hazem gamal',
            'phone' => '01110082125'
        ]);
    }
    public function test_retriveOrders()
    {
        factory('App\Category', 3)->create();
        factory('App\SubCategory', 3)->create();
        factory('App\Product', 3)->create();
        factory('App\Order')->create([
            'status' => 'cart'
        ]);
        factory('App\OrderDetails', 3)->create();

        $response = $this->actingAs($this->user)
                  ->json('GET', '/api/user/orders/get-All-Orders');

        $response->assertStatus(200);

        $this->assertCount(3, json_decode($response->getContent())->orders);
    }

    public function test_addOrder()
    {
        factory('App\Category', 3)->create();
        factory('App\SubCategory', 3)->create();
       $product = factory('App\Product')->create();

       $order =  [
        'user_id' => $this->user->id,
           'status'  => 'cart'
       ];
       $response = $this->actingAs($this->user)
                ->json('POST', "/api/user/orders/add-to-cart/{$product->id}", $order);

        $response->assertStatus(201);

        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('orders', [
            'user_id' => $order['user_id'],
            'status'  => 'cart'
        ]);

        $this->assertDatabaseHas('order_details', [
            'product_id'  => $product->id,
            'quantity'    => 1
        ]);

    }

    public function test_deleteOrder()
    {
        factory('App\Category')->create();
        factory('App\SubCategory')->create();
        factory('App\Product')->create();
        factory('App\Order')->create();
        $orderDetail = factory('App\OrderDetails')->create();


        $response = $this->actingAs($this->user)
                    ->json('DELETE', "/api/user/orders/delete-from-cart/{$orderDetail->id}");

       $response->assertStatus(201);
       $this->assertDatabaseMissing('order_details', [
           'id' => $orderDetail->id
       ]);
    }

    public function test_updateOrder()
    {
        factory('App\Category')->create();
        factory('App\SubCategory')->create();
        factory('App\Product')->create();
        $order = factory('App\Order')->create();

        $form = [
            'user_id' => $this->user->id,
            'status' => 'ordered'
        ];

        $response = $this->actingAs($this->user)
                   ->json('POST', "/api/user/orders/update-order-detail/{$order->id}", $form);
       $response->assertStatus(201);
       $this->assertDatabaseHas('orders', [
        'user_id' => $form['user_id'],
        'status'  => 'ordered'
    ]);
    }
}
