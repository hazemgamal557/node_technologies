<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    private $admin;
    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        $this->user = \App\User::create([
            'name' => 'hazem gamal',
            'phone' => '01110082125'
        ]);
    }

    public function test_retriveProduct()
    {
        factory('App\Category', 3)->create();
        factory('App\SubCategory', 3)->create();
        factory('App\Product', 3)->create();
        $response = $this->json('GET', '/api/user/product/all?limit=2');
        $response->assertStatus(200);
        $this->assertCount(2, json_decode($response->getContent()));
    }

    public function test_recommendedProduct()
    {
        factory('App\Category', 3)->create();
        factory('App\SubCategory', 3)->create();
        $product = factory('App\Product')->create();
        $response = $this->json('GET', "/api/user/product/recommend-product/{$product->id}");
        $response->assertStatus(200);
        $record = json_decode($response->getContent(), true);
    }
}
