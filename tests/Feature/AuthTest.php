<?php

namespace Tests\Feature;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    // private $admin;

    public function setUp(): void
    {
        parent::setUp();

        // $this->admin = \App\Models\Admin::create([
        //     'name'     => 'أدمن',
        //     'email'    => 'admin@admin.com',
        //     'password' => \Hash::make('secret')
        // ]);

        $this->user = \App\User::create([
            'name' => 'hazem gamal',
            'phone' => '01110082125'
        ]);
    }

    public function test_registerAndSendConfirmationCode()
    {
        $auth = [
            'phone' => '01110082125',
            'name' => 'hazem gamal',
            'address' => '325 mustafa kamel streed ghobrial',
            'image' => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300)
        ];
        $response = $this->json('POST', '/api/auth/register-and-send-confirm-code', $auth);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'name' => $auth['name'],
            'phone' => $auth['phone'],
            'confirm_code' => '1111',
            'activated_by_code' => 0,

        ]);
    }

    // un success case
    public function test_activatedConfirmCode()
    {
        $auth = [
            'phone' => '01110082125',
            'confirm_code' => '1111'
        ];
        $response = $this->json('POST', '/api/auth/activate-registered-user', $auth);
        $response->assertStatus(401);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'phone' => $auth['phone'],
            'confirm_code' => null,
            'activated_by_code' => 0
        ]);
    }

    // success case if activeConfirmCode
    public function test_activatedConfirmCodeSucess()
    {

        \DB::table('users')->where('id', $this->user->id)
            ->update([
                'confirm_code' => '1111',
                'request_confirm_code_date' => now()
                ]);
            $auth = [
                'phone' => '01110082125',
                'confirm_code' => '1111',
            ];
            $response = $this->json('POST', '/api/auth/activate-registered-user', $auth);
            $response->assertStatus(201);
            $record = json_decode($response->getContent(), true);
            $this->assertDatabaseHas('users', [
                'phone' => $auth['phone'],
                'confirm_code' => null,
                'activated_by_code' => 1,
                'request_confirm_code_date' => null
            ]);
    }

    // resend Confirmation Code
    public function test_resendConfirmationCode()
    {
        $auth = [
            'phone' => '01110082125'
        ];
        $response = $this->json('POST', '/api/auth/resend-confirm-code', $auth);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'phone' => $auth['phone'],
            'confirm_code' => '1111',
            'activated_by_code' => 0,
        ]);
    }

    // unsuccess case login
    public function test_requestLogin()
    {
        $auth = [
            'phone' => '01110082125'
        ];
        $response = $this->json('POST', '/api/auth/request-login', $auth);
        $response->assertStatus(401);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'phone' => $auth['phone'],
            'confirm_code' => null,
            'activated_by_code' => 0,
        ]);
    }

    // request login success case

    public function test_requestLoginSuccess()
    {
        \DB::table('users')->where('id', $this->user->id)->update(['activated_by_code' => 1]);
        $auth = [
            'phone' => '01110082125'
        ];
        $response = $this->json('POST', '/api/auth/request-login', $auth);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'phone' => $auth['phone'],
            'confirm_code' => '1111',
            'activated_by_code' => 1,
        ]);

    }

    //unsuccess case test login

    public function test_Login()
    {
        $auth = [
            'phone' => '01110082125',
            'confirm_code' => '1111'
        ];
        $response = $this->json('POST', '/api/auth/login', $auth);
        $response->assertStatus(401);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'phone' => $auth['phone'],
            'confirm_code' => null,
            'activated_by_code' => 0
        ]);
    }

    // success case test login
    public function test_loginSuccess()
    {
        \DB::table('users')->where('id', $this->user->id)->update([
            'activated_by_code' => 1,
            'confirm_code' => '1111',
            'request_confirm_code_date' => now()
        ]);

        $auth = [
            'phone' => '01110082125',
            'confirm_code' => '1111'
        ];
        $response = $this->json('POST', '/api/auth/login', $auth);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'phone' => $auth['phone'],
            'confirm_code' => null,
            'activated_by_code' => 1
        ]);

    }

    public function test_Logout()
    {
        \DB::table('users')
        ->where('id', $this->user->id)
        ->update([
            'activated_by_code' => 1
        ]);
        $response = $this->actingAs($this->user)->json('POST', '/api/auth/logout');
        // dd($response);
        $response->assertStatus(200);
    }

    public function test_sendUpdatePhoneConfirmationCode()
    {
        \DB::table('users')
        ->where('id', $this->user->id)
        ->update([
            'activated_by_code' => 1
        ]);

        $auth = [
            'phone' => '010061981035'
        ];

        $response = $this->actingAs($this->user)->json('POST', '/api/auth/send-update-phone-confirm-code' , $auth);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'phone' => '01110082125',
            'confirm_code' => '1111',
            'request_new_phone' => $auth['phone'],
            'activated_by_code' => '1'

        ]);
    }

    public function test_updateAddressAndImage()
    {
        \DB::table('users')
        ->where('id', $this->user->id)
        ->update([
            'activated_by_code' => 1
        ]);

        $auth = [
            'address' => '325 mustafa kamel street',
            'image' => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300)
        ];

        $response = $this->actingAs($this->user)->json('POST', '/api/auth/update-image-and-address', $auth);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'address' => $auth['address'],
        ]);
    }

    public function test_checkConfirmationCodeAndUpdatePhone()
    {
        $auth = [
            'confirm_code' => '1111'
        ];
        $response = $this->actingAs($this->user)->json('POST','/api/auth/check-confirm-code-and-update-phone',$auth);
        $response->assertStatus(410);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'phone' => '01110082125',
            'request_new_phone' => null,
            'confirm_code' => null,
            'request_confirm_code_date' => null,
            'activated_by_code' => 0

        ]);
    }

    // check confirm code success case
    public function test_checkConfirmationCodeAndUpdatePhoneSuccess()
    {
        $this->user->confirm_code = '1111';
        $this->user->request_confirm_code_date = now();
        $this->user->request_new_phone = '012335925492';
        $this->user->save();
        $auth = [
            'confirm_code' => '1111'
        ];
        $response = $this->actingAs($this->user)->json('POST', '/api/auth/check-confirm-code-and-update-phone', $auth);
        $response->assertStatus(201);
        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('users', [
            'phone' => '012335925492',
            'confirm_code' => null,
            'request_new_phone' => null
        ]);

    }
    public function test_ShowProfile()
    {
        \DB::table('users')
        ->where('id', $this->user->id)
        ->update([
            'activated_by_code' => 1
        ]);
        $response = $this->actingAs($this->user)->json('GET', '/api/auth/show-profile');
        $response->assertStatus(200);
        $record = json_decode($response->getContent(), true);
    }

}
