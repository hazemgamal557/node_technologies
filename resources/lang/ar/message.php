<?php
return [
    'confirm-code-sent' => 'تم ارسال الرقم التاكيدى بنجاح',
    'no active user has this phone number' => 'لا يوجد مستخدم نشط لديه هذا الهاتف',
    'this code is expired please try again' => 'صلاحية هذا الرقم منتهية لمرور دقيقة من استقبال الكود',
    'this confirm code is not right please try agian' => 'الرقم التاكيدى غير صحيح من فضلك حاول التسجيل مرة اخرى',
    'signup successfully' => 'تم انشاء الحساب بنجاح',
    'activated code successfully' => 'تم تفعيل الحساب بنجاح',
    'loggin successfully' => 'تسجيل دخول ناجح',
    'logged out' => 'تم تسجيل الخروج بنجاح',
    'updated-sucessfully' => 'تم تحديث الهاتف بنجاح',
    'your order has been addedd successfully added' => 'تمت اضافة طلبك بنجاح',
    'sorry you are not authorized to updated this orderes' => 'عذرا ليس لديك اي صلاحية لاضافة هذا الطلب',
    'order send successfully' => 'تم وصول طلبك بنجاح',
    'your quantity updated successfully' => 'تم تحديث كميه الطلب بنجاح',
    'your order is deleted' => 'تم حذف الطلب بنجاح',
    'unauthourized to delete this order' => 'ليس لديك صلاحية لحذف هذا الطلب',
    'updated-language-sucessfully' => 'تم تحديث اللغه بنجاح',
    'updated-user-data-sucessfully' => 'تم تحديث بياناتك بنجاح'

];
