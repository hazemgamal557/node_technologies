<?php
return [
    'confirm-code-sent' => 'confirm-code-sent',
    'no active user has this phone number' => 'no active user has this phone number',
    'this code is expired please try again' => 'this code is expired please try again',
    'this confirm code is not right please try agian' => 'this confirm code is not right please try agian',
    'signup successfully' => 'signup successfully',
    'activated code successfully' => 'activated code successfully',
    'loggin successfully' => 'loggin successfully',
    'logged out' => 'logged out',
    'updated-sucessfully' => 'updated-sucessfully',
    'your order has been addedd successfully added' => 'your order has been addedd successfully added',
    'sorry you are not authorized to updated this orderes' => 'sorry you are not authorized to updated this orderes',
    'order send successfully' => 'order send successfully',
    'your quantity updated successfully' => 'your quantity updated successfully',
    'your order is deleted' => 'your order is deleted',
    'unauthourized to delete this order' => 'unauthourized to delete this order',
    'updated-language-sucessfully' => 'updated-language-sucessfully',
    'updated-user-data-sucessfully' => 'updated-user-data-sucessfully',
];
