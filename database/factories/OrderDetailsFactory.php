<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderDetails;
use App\Order;
use App\Product;
use Faker\Generator as Faker;

$factory->define(OrderDetails::class, function (Faker $faker) {
    return [
        'order_id' => function()
        {
            return Order::all()->random();
        },
        'product_id' => function()
        {
            return Product::all()->random();
        },
        'price' => '55',
        'quantity' => '6'
    ];
});
