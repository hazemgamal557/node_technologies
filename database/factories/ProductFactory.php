<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\SubCategory;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name_ar' => $faker->word,
        'name_en' => $faker->word,
        'trademark_ar' => $faker->word,
        'trademark_en' => $faker->word,
        'type_ar' => $faker->word,
        'type_en' => $faker->word,
        'type_ar' => $faker->word,
        'description_ar' => $faker->word,
        'description_en' => $faker->word,
        'price' => $faker->numberBetween($min = 20, $max = 300),
        'sub_categories_id' => function()
        {
            return SubCategory::all()->random();
        }
    ];
});
