<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\ProductMedia;
use Faker\Generator as Faker;

$factory->define(ProductMedia::class, function (Faker $faker) {
    return [
        'image' => '',
        'caption' => 1,
        'product_id' => function()
        {
            return Product::all()->random();
        }
    ];
});
