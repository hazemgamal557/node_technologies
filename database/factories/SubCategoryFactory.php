<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SubCategory;
use App\Category;
use Faker\Generator as Faker;

$factory->define(SubCategory::class, function (Faker $faker) {
    return [
           'name_ar' => $faker->word,
           'name_en' => $faker->word,
           'image' => '',
         'category_id' => function()
        {
            return Category::all()->random();
        }
    ];
});
