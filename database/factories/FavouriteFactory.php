<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Favourite;
use App\User;
use App\Product;
use Faker\Generator as Faker;

$factory->define(Favourite::class, function (Faker $faker) {
    return [
        'user_id' => function()
        {
            return User::all()->random();
        },
        'product_id' => function()
        {
            return Product::all()->random();
        },
    ];
});
