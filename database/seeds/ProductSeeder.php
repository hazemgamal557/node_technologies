<?php

use Illuminate\Database\Seeder;
use App\Order;
use App\User;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedProduct();
    }

    private function seedProduct()
    {
        $records = [
            [   
                'name_en'                   =>  'electric cables 1',
                'name_ar'                   =>  'كابلات كهربائية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات كهربائية',
                'description_en'            =>  'this is electric cables',
                'price'                     => 55,
                'sub_categories_id'         => 1

            ],
            [   
                'name_en'                   =>  'electric cables 2',
                'name_ar'                   =>  'كابلات كهربائية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات كهربائية',
                'description_en'            =>  'this is electric cables',
                'price'                     => 55,
                'sub_categories_id'         => 1

            ],
            [   
                'name_en'                   =>  'electric cables 3',
                'name_ar'                   =>  'كابلات كهربائية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات كهربائية',
                'description_en'            =>  'this is electric cables',
                'price'                     => 55,
                'sub_categories_id'         => 1

            ],
            [   
                'name_en'                   =>  'electric cables 4',
                'name_ar'                   =>  'كابلات كهربائية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات كهربائية',
                'description_en'            =>  'this is electric cables',
                'price'                     => 55,
                'sub_categories_id'         => 1

            ],
            [   
                'name_en'                   =>  'electric cables 5',
                'name_ar'                   =>  'كابلات كهربائية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات كهربائية',
                'description_en'            =>  'this is electric cables',
                'price'                     => 55,
                'sub_categories_id'         => 1

            ],




            [   
                'name_en'                   =>  'metal cables 1',
                'name_ar'                   =>  'كابلات معدنية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات معدنية',
                'description_en'            =>  'this is metal cables',
                'price'                     => 120,
                'sub_categories_id'         => 2

            ],
            [   
                'name_en'                   =>  'metal cables 2',
                'name_ar'                   =>  'كابلات معدنية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات معدنية',
                'description_en'            =>  'this is metal cables',
                'price'                     => 120,
                'sub_categories_id'         => 2

            ],
            [   
                'name_en'                   =>  'metal cables 3',
                'name_ar'                   =>  'كابلات معدنية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات معدنية',
                'description_en'            =>  'this is metal cables',
                'price'                     => 120,
                'sub_categories_id'         => 2

            ],
            [   
                'name_en'                   =>  'metal cables 4',
                'name_ar'                   =>  'كابلات معدنية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات معدنية',
                'description_en'            =>  'this is metal cables',
                'price'                     => 120,
                'sub_categories_id'         => 2

            ],
            [   
                'name_en'                   =>  'metal cables 5',
                'name_ar'                   =>  'كابلات معدنية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات معدنية',
                'description_en'            =>  'this is metal cables',
                'price'                     => 120,
                'sub_categories_id'         => 2

            ],


            [   
                'name_en'                   =>  'copper cables 1',
                'name_ar'                   =>  'كابلات نحاسية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات نحاسية',
                'description_en'            =>  'this is copper cables',
                'price'                     => 350,
                'sub_categories_id'         => 3

            ],

            [   
                'name_en'                   =>  'copper cables 2',
                'name_ar'                   =>  'كابلات نحاسية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات نحاسية',
                'description_en'            =>  'this is copper cables',
                'price'                     => 350,
                'sub_categories_id'         => 3

            ],

            [   
                'name_en'                   =>  'copper cables 3',
                'name_ar'                   =>  'كابلات نحاسية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات نحاسية',
                'description_en'            =>  'this is copper cables',
                'price'                     => 350,
                'sub_categories_id'         => 3

            ],

            [   
                'name_en'                   =>  'copper cables 4',
                'name_ar'                   =>  'كابلات نحاسية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات نحاسية',
                'description_en'            =>  'this is copper cables',
                'price'                     => 350,
                'sub_categories_id'         => 3

            ],

            [   
                'name_en'                   =>  'copper cables 5',
                'name_ar'                   =>  'كابلات نحاسية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات نحاسية',
                'description_en'            =>  'this is copper cables',
                'price'                     => 350,
                'sub_categories_id'         => 3

            ],


            
            [   
                'name_en'                   =>  'plastic cables 1',
                'name_ar'                   =>  'كابلات بلاستيكية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات بلاستيكية',
                'description_en'            =>  'this is plastic cables',
                'price'                     => 240,
                'sub_categories_id'         => 4

            ],
            [   
                'name_en'                   =>  'plastic cables 2',
                'name_ar'                   =>  'كابلات بلاستيكية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات بلاستيكية',
                'description_en'            =>  'this is plastic cables',
                'price'                     => 240,
                'sub_categories_id'         => 4

            ],
            [   
                'name_en'                   =>  'plastic cables 3',
                'name_ar'                   =>  'كابلات بلاستيكية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات بلاستيكية',
                'description_en'            =>  'this is plastic cables',
                'price'                     => 240,
                'sub_categories_id'         => 4

            ],
            [   
                'name_en'                   =>  'plastic cables 4',
                'name_ar'                   =>  'كابلات بلاستيكية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات بلاستيكية',
                'description_en'            =>  'this is plastic cables',
                'price'                     => 240,
                'sub_categories_id'         => 4

            ],
            [   
                'name_en'                   =>  'plastic cables 5',
                'name_ar'                   =>  'كابلات بلاستيكية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'اسلاك',
                'type_en'                   =>  'cables',
                'description_ar'            => 'هذه الكابلات بلاستيكية',
                'description_en'            =>  'this is plastic cables',
                'price'                     => 240,
                'sub_categories_id'         => 4

            ],


























             [   
                'name_en'                   =>  'electric joints 1',
                'name_ar'                   =>  'مشتركات كهربائية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات كهربائية',
                'description_en'            =>  'this is electric joints',
                'price'                     => 55,
                'sub_categories_id'         => 5

            ],
            [   
                'name_en'                   =>  'electric joints 2',
                'name_ar'                   =>  'مشتركات كهربائية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات كهربائية',
                'description_en'            =>  'this is electric joints',
                'price'                     => 55,
                'sub_categories_id'         => 5

            ],
            [   
                'name_en'                   =>  'electric joints 3',
                'name_ar'                   =>  'مشتركات كهربائية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات كهربائية',
                'description_en'            =>  'this is electric joints',
                'price'                     => 55,
                'sub_categories_id'         => 5

            ],
            [   
                'name_en'                   =>  'electric joints 4',
                'name_ar'                   =>  'مشتركات كهربائية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات كهربائية',
                'description_en'            =>  'this is electric joints',
                'price'                     => 55,
                'sub_categories_id'         => 5

            ],
            [   
                'name_en'                   =>  'electric joints 5',
                'name_ar'                   =>  'مشتركات كهربائية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات كهربائية',
                'description_en'            =>  'this is electric joints',
                'price'                     => 55,
                'sub_categories_id'         => 5

            ],




            [   
                'name_en'                   =>  'metal joints 1',
                'name_ar'                   =>  'مشتركات معدنية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات معدنية',
                'description_en'            =>  'this is metal joints',
                'price'                     => 120,
                'sub_categories_id'         => 6

            ],
            [   
                'name_en'                   =>  'metal joints 2',
                'name_ar'                   =>  'مشتركات معدنية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات معدنية',
                'description_en'            =>  'this is metal joints',
                'price'                     => 120,
                'sub_categories_id'         => 6

            ],
            [   
                'name_en'                   =>  'metal joints 3',
                'name_ar'                   =>  'مشتركات معدنية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات معدنية',
                'description_en'            =>  'this is metal joints',
                'price'                     => 120,
                'sub_categories_id'         => 6

            ],
            [   
                'name_en'                   =>  'metal joints 4',
                'name_ar'                   =>  'مشتركات معدنية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات معدنية',
                'description_en'            =>  'this is metal joints',
                'price'                     => 120,
                'sub_categories_id'         => 6

            ],
            [   
                'name_en'                   =>  'metal joints 5',
                'name_ar'                   =>  'مشتركات معدنية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات معدنية',
                'description_en'            =>  'this is metal joints',
                'price'                     => 120,
                'sub_categories_id'         => 6

            ],


            [   
                'name_en'                   =>  'copper joints 1',
                'name_ar'                   =>  'مشتركات نحاسية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات نحاسية',
                'description_en'            =>  'this is copper joints',
                'price'                     => 350,
                'sub_categories_id'         => 7

            ],

            [   
                'name_en'                   =>  'copper joints 2',
                'name_ar'                   =>  'مشتركات نحاسية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات نحاسية',
                'description_en'            =>  'this is copper joints',
                'price'                     => 350,
                'sub_categories_id'         => 7

            ],

            [   
                'name_en'                   =>  'copper joints 3',
                'name_ar'                   =>  'مشتركات نحاسية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات نحاسية',
                'description_en'            =>  'this is copper joints',
                'price'                     => 350,
                'sub_categories_id'         => 7

            ],

            [   
                'name_en'                   =>  'copper joints 4',
                'name_ar'                   =>  'مشتركات نحاسية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات نحاسية',
                'description_en'            =>  'this is copper joints',
                'price'                     => 350,
                'sub_categories_id'         => 7

            ],

            [   
                'name_en'                   =>  'copper joints 5',
                'name_ar'                   =>  'مشتركات نحاسية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات نحاسية',
                'description_en'            =>  'this is copper joints',
                'price'                     => 350,
                'sub_categories_id'         => 7

            ],


            
            [   
                'name_en'                   =>  'plastic joints 1',
                'name_ar'                   =>  'مشتركات بلاستيكية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات بلاستيكية',
                'description_en'            =>  'this is plastic joints',
                'price'                     => 240,
                'sub_categories_id'         => 8

            ],
            [   
                'name_en'                   =>  'plastic joints 2',
                'name_ar'                   =>  'مشتركات بلاستيكية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات بلاستيكية',
                'description_en'            =>  'this is plastic joints',
                'price'                     => 240,
                'sub_categories_id'         => 8

            ],
            [   
                'name_en'                   =>  'plastic joints 3',
                'name_ar'                   =>  'مشتركات بلاستيكية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات بلاستيكية',
                'description_en'            =>  'this is plastic joints',
                'price'                     => 240,
                'sub_categories_id'         => 8

            ],
            [   
                'name_en'                   =>  'plastic joints 4',
                'name_ar'                   =>  'مشتركات بلاستيكية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات بلاستيكية',
                'description_en'            =>  'this is plastic joints',
                'price'                     => 240,
                'sub_categories_id'         => 8

            ],
            [   
                'name_en'                   =>  'plastic joints 5',
                'name_ar'                   =>  'مشتركات بلاستيكية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مشتركات',
                'type_en'                   =>  'joints',
                'description_ar'            => 'هذه المشتركات بلاستيكية',
                'description_en'            =>  'this is plastic joints',
                'price'                     => 240,
                'sub_categories_id'         => 8

            ],


















            [   
                'name_en'                   =>  'electric screwdriver 1',
                'name_ar'                   =>  'مفكات كهربائية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات كهربائية',
                'description_en'            =>  'this is electric screwdriver',
                'price'                     => 55,
                'sub_categories_id'         => 9

            ],
            [   
                'name_en'                   =>  'electric screwdriver 2',
                'name_ar'                   =>  'مفكات كهربائية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات كهربائية',
                'description_en'            =>  'this is electric screwdriver',
                'price'                     => 55,
                'sub_categories_id'         => 9

            ],
            [   
                'name_en'                   =>  'electric screwdriver 3',
                'name_ar'                   =>  'مفكات كهربائية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات كهربائية',
                'description_en'            =>  'this is electric screwdriver',
                'price'                     => 55,
                'sub_categories_id'         => 9

            ],
            [   
                'name_en'                   =>  'electric screwdriver 4',
                'name_ar'                   =>  'مفكات كهربائية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات كهربائية',
                'description_en'            =>  'this is electric screwdriver',
                'price'                     => 55,
                'sub_categories_id'         => 9

            ],
            [   
                'name_en'                   =>  'electric screwdriver 5',
                'name_ar'                   =>  'مفكات كهربائية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات كهربائية',
                'description_en'            =>  'this is electric screwdriver',
                'price'                     => 55,
                'sub_categories_id'         => 9

            ],




            [   
                'name_en'                   =>  'metal screwdriver 1',
                'name_ar'                   =>  'مفكات معدنية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات معدنية',
                'description_en'            =>  'this is metal screwdriver',
                'price'                     => 120,
                'sub_categories_id'         => 10

            ],
            [   
                'name_en'                   =>  'metal screwdriver 2',
                'name_ar'                   =>  'مفكات معدنية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات معدنية',
                'description_en'            =>  'this is metal screwdriver',
                'price'                     => 120,
                'sub_categories_id'         => 10

            ],
            [   
                'name_en'                   =>  'metal screwdriver 3',
                'name_ar'                   =>  'مفكات معدنية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات معدنية',
                'description_en'            =>  'this is metal screwdriver',
                'price'                     => 120,
                'sub_categories_id'         => 10

            ],
            [   
                'name_en'                   =>  'metal screwdriver 4',
                'name_ar'                   =>  'مفكات معدنية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات معدنية',
                'description_en'            =>  'this is metal screwdriver',
                'price'                     => 120,
                'sub_categories_id'         => 10

            ],
            [   
                'name_en'                   =>  'metal screwdriver 5',
                'name_ar'                   =>  'مفكات معدنية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات معدنية',
                'description_en'            =>  'this is metal screwdriver',
                'price'                     => 120,
                'sub_categories_id'         => 10

            ],


            [   
                'name_en'                   =>  'copper screwdriver 1',
                'name_ar'                   =>  'مفكات نحاسية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات نحاسية',
                'description_en'            =>  'this is copper screwdriver',
                'price'                     => 350,
                'sub_categories_id'         => 11

            ],

            [   
                'name_en'                   =>  'copper screwdriver 2',
                'name_ar'                   =>  'مفكات نحاسية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات نحاسية',
                'description_en'            =>  'this is copper screwdriver',
                'price'                     => 350,
                'sub_categories_id'         => 11

            ],

            [   
                'name_en'                   =>  'copper screwdriver 3',
                'name_ar'                   =>  'مفكات نحاسية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات نحاسية',
                'description_en'            =>  'this is copper screwdriver',
                'price'                     => 350,
                'sub_categories_id'         => 11

            ],

            [   
                'name_en'                   =>  'copper screwdriver 4',
                'name_ar'                   =>  'مفكات نحاسية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات نحاسية',
                'description_en'            =>  'this is copper screwdriver',
                'price'                     => 350,
                'sub_categories_id'         => 11

            ],

            [   
                'name_en'                   =>  'copper screwdriver 5',
                'name_ar'                   =>  'مفكات نحاسية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات نحاسية',
                'description_en'            =>  'this is copper screwdriver',
                'price'                     => 350,
                'sub_categories_id'         => 11

            ],


            
            [   
                'name_en'                   =>  'plastic screwdriver 1',
                'name_ar'                   =>  'مفكات بلاستيكية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات بلاستيكية',
                'description_en'            =>  'this is plastic screwdriver',
                'price'                     => 240,
                'sub_categories_id'         => 12

            ],
            [   
                'name_en'                   =>  'plastic screwdriver 2',
                'name_ar'                   =>  'مفكات بلاستيكية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات بلاستيكية',
                'description_en'            =>  'this is plastic screwdriver',
                'price'                     => 240,
                'sub_categories_id'         => 12

            ],
            [   
                'name_en'                   =>  'plastic screwdriver 3',
                'name_ar'                   =>  'مفكات بلاستيكية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات بلاستيكية',
                'description_en'            =>  'this is plastic screwdriver',
                'price'                     => 240,
                'sub_categories_id'         => 12

            ],
            [   
                'name_en'                   =>  'plastic screwdriver 4',
                'name_ar'                   =>  'مفكات بلاستيكية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات بلاستيكية',
                'description_en'            =>  'this is plastic screwdriver',
                'price'                     => 240,
                'sub_categories_id'         => 12

            ],
            [   
                'name_en'                   =>  'plastic screwdriver 5',
                'name_ar'                   =>  'مفكات بلاستيكية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'مفكات',
                'type_en'                   =>  'screwdriver',
                'description_ar'            => 'هذه المفكات بلاستيكية',
                'description_en'            =>  'this is plastic screwdriver',
                'price'                     => 240,
                'sub_categories_id'         => 12

            ],
















            [   
                'name_en'                   =>  'electric tapes 1',
                'name_ar'                   =>  'شرايط كهربائية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط كهربائية',
                'description_en'            =>  'this is electric tapes',
                'price'                     => 55,
                'sub_categories_id'         => 13

            ],
            [   
                'name_en'                   =>  'electric tapes 2',
                'name_ar'                   =>  'شرايط كهربائية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط كهربائية',
                'description_en'            =>  'this is electric tapes',
                'price'                     => 55,
                'sub_categories_id'         => 13

            ],
            [   
                'name_en'                   =>  'electric tapes 3',
                'name_ar'                   =>  'شرايط كهربائية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط كهربائية',
                'description_en'            =>  'this is electric tapes',
                'price'                     => 55,
                'sub_categories_id'         => 13

            ],
            [   
                'name_en'                   =>  'electric tapes 4',
                'name_ar'                   =>  'شرايط كهربائية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط كهربائية',
                'description_en'            =>  'this is electric tapes',
                'price'                     => 55,
                'sub_categories_id'         => 13

            ],
            [   
                'name_en'                   =>  'electric tapes 5',
                'name_ar'                   =>  'شرايط كهربائية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط كهربائية',
                'description_en'            =>  'this is electric tapes',
                'price'                     => 55,
                'sub_categories_id'         => 13

            ],




            [   
                'name_en'                   =>  'metal tapes 1',
                'name_ar'                   =>  'شرايط معدنية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط معدنية',
                'description_en'            =>  'this is metal tapes',
                'price'                     => 120,
                'sub_categories_id'         => 14

            ],
            [   
                'name_en'                   =>  'metal tapes 2',
                'name_ar'                   =>  'شرايط معدنية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط معدنية',
                'description_en'            =>  'this is metal tapes',
                'price'                     => 120,
                'sub_categories_id'         => 14

            ],
            [   
                'name_en'                   =>  'metal tapes 3',
                'name_ar'                   =>  'شرايط معدنية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط معدنية',
                'description_en'            =>  'this is metal tapes',
                'price'                     => 120,
                'sub_categories_id'         => 14

            ],
            [   
                'name_en'                   =>  'metal tapes 4',
                'name_ar'                   =>  'شرايط معدنية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط معدنية',
                'description_en'            =>  'this is metal tapes',
                'price'                     => 120,
                'sub_categories_id'         => 14

            ],
            [   
                'name_en'                   =>  'metal tapes 5',
                'name_ar'                   =>  'شرايط معدنية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط معدنية',
                'description_en'            =>  'this is metal tapes',
                'price'                     => 120,
                'sub_categories_id'         => 14

            ],


            [   
                'name_en'                   =>  'copper tapes 1',
                'name_ar'                   =>  'شرايط نحاسية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط نحاسية',
                'description_en'            =>  'this is copper tapes',
                'price'                     => 350,
                'sub_categories_id'         => 15

            ],

            [   
                'name_en'                   =>  'copper tapes 2',
                'name_ar'                   =>  'شرايط نحاسية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط نحاسية',
                'description_en'            =>  'this is copper tapes',
                'price'                     => 350,
                'sub_categories_id'         => 15

            ],

            [   
                'name_en'                   =>  'copper tapes 3',
                'name_ar'                   =>  'شرايط نحاسية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط نحاسية',
                'description_en'            =>  'this is copper tapes',
                'price'                     => 350,
                'sub_categories_id'         => 15

            ],

            [   
                'name_en'                   =>  'copper tapes 4',
                'name_ar'                   =>  'شرايط نحاسية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط نحاسية',
                'description_en'            =>  'this is copper tapes',
                'price'                     => 350,
                'sub_categories_id'         => 15

            ],

            [   
                'name_en'                   =>  'copper tapes 5',
                'name_ar'                   =>  'شرايط نحاسية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط نحاسية',
                'description_en'            =>  'this is copper tapes',
                'price'                     => 350,
                'sub_categories_id'         => 15

            ],


            
            [   
                'name_en'                   =>  'plastic tapes 1',
                'name_ar'                   =>  'شرايط بلاستيكية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط بلاستيكية',
                'description_en'            =>  'this is plastic tapes',
                'price'                     => 240,
                'sub_categories_id'         => 16

            ],
            [   
                'name_en'                   =>  'plastic tapes 2',
                'name_ar'                   =>  'شرايط بلاستيكية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط بلاستيكية',
                'description_en'            =>  'this is plastic tapes',
                'price'                     => 240,
                'sub_categories_id'         => 16

            ],
            [   
                'name_en'                   =>  'plastic tapes 3',
                'name_ar'                   =>  'شرايط بلاستيكية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط بلاستيكية',
                'description_en'            =>  'this is plastic tapes',
                'price'                     => 240,
                'sub_categories_id'         => 16

            ],
            [   
                'name_en'                   =>  'plastic tapes 4',
                'name_ar'                   =>  'شرايط بلاستيكية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط بلاستيكية',
                'description_en'            =>  'this is plastic tapes',
                'price'                     => 240,
                'sub_categories_id'         => 16

            ],
            [   
                'name_en'                   =>  'plastic tapes 5',
                'name_ar'                   =>  'شرايط بلاستيكية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'شرايط',
                'type_en'                   =>  'tapes',
                'description_ar'            => 'هذه الشرايط بلاستيكية',
                'description_en'            =>  'this is plastic tapes',
                'price'                     => 240,
                'sub_categories_id'         => 16

            ],














            [   
                'name_en'                   =>  'electric generator 1',
                'name_ar'                   =>  'محولات كهربائية كهربائية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية كهربائية',
                'description_en'            =>  'this is electric generator',
                'price'                     => 55,
                'sub_categories_id'         => 17

            ],
            [   
                'name_en'                   =>  'electric generator 2',
                'name_ar'                   =>  'محولات كهربائية كهربائية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية كهربائية',
                'description_en'            =>  'this is electric generator',
                'price'                     => 55,
                'sub_categories_id'         => 17

            ],
            [   
                'name_en'                   =>  'electric generator 3',
                'name_ar'                   =>  'محولات كهربائية كهربائية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية كهربائية',
                'description_en'            =>  'this is electric generator',
                'price'                     => 55,
                'sub_categories_id'         => 17

            ],
            [   
                'name_en'                   =>  'electric generator 4',
                'name_ar'                   =>  'محولات كهربائية كهربائية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية كهربائية',
                'description_en'            =>  'this is electric generator',
                'price'                     => 55,
                'sub_categories_id'         => 17

            ],
            [   
                'name_en'                   =>  'electric generator 5',
                'name_ar'                   =>  'محولات كهربائية كهربائية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية كهربائية',
                'description_en'            =>  'this is electric generator',
                'price'                     => 55,
                'sub_categories_id'         => 17

            ],




            [   
                'name_en'                   =>  'metal generator 1',
                'name_ar'                   =>  'محولات كهربائية معدنية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية معدنية',
                'description_en'            =>  'this is metal generator',
                'price'                     => 120,
                'sub_categories_id'         => 18

            ],
            [   
                'name_en'                   =>  'metal generator 2',
                'name_ar'                   =>  'محولات كهربائية معدنية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية معدنية',
                'description_en'            =>  'this is metal generator',
                'price'                     => 120,
                'sub_categories_id'         => 18

            ],
            [   
                'name_en'                   =>  'metal generator 3',
                'name_ar'                   =>  'محولات كهربائية معدنية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية معدنية',
                'description_en'            =>  'this is metal generator',
                'price'                     => 120,
                'sub_categories_id'         => 18

            ],
            [   
                'name_en'                   =>  'metal generator 4',
                'name_ar'                   =>  'محولات كهربائية معدنية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية معدنية',
                'description_en'            =>  'this is metal generator',
                'price'                     => 120,
                'sub_categories_id'         => 18

            ],
            [   
                'name_en'                   =>  'metal generator 5',
                'name_ar'                   =>  'محولات كهربائية معدنية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية معدنية',
                'description_en'            =>  'this is metal generator',
                'price'                     => 120,
                'sub_categories_id'         => 18

            ],


            [   
                'name_en'                   =>  'copper generator 1',
                'name_ar'                   =>  'محولات كهربائية نحاسية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية نحاسية',
                'description_en'            =>  'this is copper generator',
                'price'                     => 350,
                'sub_categories_id'         => 19

            ],

            [   
                'name_en'                   =>  'copper generator 2',
                'name_ar'                   =>  'محولات كهربائية نحاسية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية نحاسية',
                'description_en'            =>  'this is copper generator',
                'price'                     => 350,
                'sub_categories_id'         => 19

            ],

            [   
                'name_en'                   =>  'copper generator 3',
                'name_ar'                   =>  'محولات كهربائية نحاسية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية نحاسية',
                'description_en'            =>  'this is copper generator',
                'price'                     => 350,
                'sub_categories_id'         => 19

            ],

            [   
                'name_en'                   =>  'copper generator 4',
                'name_ar'                   =>  'محولات كهربائية نحاسية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية نحاسية',
                'description_en'            =>  'this is copper generator',
                'price'                     => 350,
                'sub_categories_id'         => 19

            ],

            [   
                'name_en'                   =>  'copper generator 5',
                'name_ar'                   =>  'محولات كهربائية نحاسية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية نحاسية',
                'description_en'            =>  'this is copper generator',
                'price'                     => 350,
                'sub_categories_id'         => 19

            ],


            
            [   
                'name_en'                   =>  'plastic generator 1',
                'name_ar'                   =>  'محولات كهربائية بلاستيكية 1',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية بلاستيكية',
                'description_en'            =>  'this is plastic generator',
                'price'                     => 240,
                'sub_categories_id'         => 20

            ],
            [   
                'name_en'                   =>  'plastic generator 2',
                'name_ar'                   =>  'محولات كهربائية بلاستيكية 2',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية بلاستيكية',
                'description_en'            =>  'this is plastic generator',
                'price'                     => 240,
                'sub_categories_id'         => 20

            ],
            [   
                'name_en'                   =>  'plastic generator 3',
                'name_ar'                   =>  'محولات كهربائية بلاستيكية 3',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية بلاستيكية',
                'description_en'            =>  'this is plastic generator',
                'price'                     => 240,
                'sub_categories_id'         => 20

            ],
            [   
                'name_en'                   =>  'plastic generator 4',
                'name_ar'                   =>  'محولات كهربائية بلاستيكية 4',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية بلاستيكية',
                'description_en'            =>  'this is plastic generator',
                'price'                     => 240,
                'sub_categories_id'         => 20

            ],
            [   
                'name_en'                   =>  'plastic generator 5',
                'name_ar'                   =>  'محولات كهربائية بلاستيكية 5',
                'trademark_ar'              =>  'العلامة التجارية',
                'trademark_en'              =>  'trademark',
                'type_ar'                   =>  'محولات كهربائية',
                'type_en'                   =>  'generator',
                'description_ar'            => 'هذه المحولات كهربائية بلاستيكية',
                'description_en'            =>  'this is plastic generator',
                'price'                     => 240,
                'sub_categories_id'         => 20

            ],


        ];
        \App\Product::insert($records);
        echo " 👍\n";
    }
}
