<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "🚦 start seeding CategorySeeder\n";
        $this->seedCategory();
    }
    private function seedCategory()
    {
        echo "🕛 categories";
        $records = [
            [
                'id' => 1,
                'name_ar' => 'كابلات',
                'name_en' => 'Cables',
                'image' => 'default/category/init/cable.png',
            ],
            [
                'id' => 2,
                'name_ar' => 'مشتركات',
                'name_en' => 'Joints',
                'image' => 'default/category/init/joint.png',
            ],
            [
                'id' => 3,
                'name_ar' => 'مفكات',
                'name_en' => 'Screwdrivers',
                'image' => 'default/category/init/screwdriver.png',
            ],
            [
                'id' => 4,
                'name_ar' => 'شرايط',
                'name_en' => 'Tapes',
                'image' => 'default/category/init/tape.png',
            ],
            [
                'id' => 5,
                'name_ar' => 'محولات كهربائية',
                'name_en' => 'Generators',
                'image' => 'default/category/init/generator.png',
            ],

        ];
        \App\Category::insert($records);
        echo " 👍\n";
    }
}
