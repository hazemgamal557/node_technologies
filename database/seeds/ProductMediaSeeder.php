<?php

use Illuminate\Database\Seeder;

class ProductMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedProductMedia();
    }
    private function seedProductMedia()
    {
        $records = [
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 1,
                'product_id' => 1,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 0,
                'product_id' => 1,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 1,
                'product_id' => 2,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 0,
                'product_id' => 2,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 3,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 3,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 4,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 4,
            ],

            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 5,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 0,
                'product_id' => 5,
            ],

            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 6,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 6,
            ],
             [
                'image' => 'default/product/init/cable.png',
                'caption' => 1,
                'product_id' => 7,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 0,
                'product_id' => 7,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 8,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 8,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 1,
                'product_id' => 9,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 0,
                'product_id' => 9,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 10,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 10,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 11,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 11,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 12,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 12,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 13,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 13,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 14,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 14,
            ], [
                'image' => 'default/product/init/cable.png',
                'caption' => 1,
                'product_id' => 15,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 0,
                'product_id' => 15,
            ], [
                'image' => 'default/product/init/cable.png',
                'caption' => 1,
                'product_id' => 16,
            ],
            [
                'image' => 'default/product/init/cable1.png',
                'caption' => 0,
                'product_id' => 16,
            ], [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 17,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 17,
            ], [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 18,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 18,
            ], [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 19,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 19,
            ], [
                'image' => 'default/product/init/cable1.png',
                'caption' => 1,
                'product_id' => 20,
            ],
            [
                'image' => 'default/product/init/cable.png',
                'caption' => 0,
                'product_id' => 20,
            ],
            
            
            
            
            [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 21,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 21,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 22,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 22,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 23,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 23,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 24,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 24,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 25,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 25,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 26,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 26,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 27,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 27,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 28,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 28,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 29,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 29,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 30,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 30,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 31,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 31,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 32,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 32,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 33,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 33,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 34,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 34,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 35,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 35,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 36,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 36,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 37,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 37,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 38,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 38,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 39,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 39,
            ], [
                'image' => 'default/product/init/joint.png',
                'caption' => 1,
                'product_id' => 40,
            ],
            [
                'image' => 'default/product/init/joint1.png',
                'caption' => 0,
                'product_id' => 40,
            ],
            
            
            
            
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 41,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 41,
            ],
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 42,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 42,
            ],
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 43,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 43,
            ],
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 44,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 44,
            ],
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 45,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 45,
            ],
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 46,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 46,
            ],
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 47,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 47,
            ],
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 48,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 48,
            ],
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 49,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 49,
            ],
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 50,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 50,
            ],
            [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 51,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 51,
            ], [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 52,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 52,
            ], [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 53,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 53,
            ], [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 54,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 54,
            ], [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 55,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 55,
            ], [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 56,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 56,
            ], [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 57,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 57,
            ], [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 58,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 58,
            ], [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 59,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 59,
            ], [
                'image' => 'default/product/init/screwdriver.png',
                'caption' => 1,
                'product_id' => 60,
            ],
            [
                'image' => 'default/product/init/screwdriver1.png',
                'caption' => 0,
                'product_id' => 60,
            ], 
            
            
            
            
            
            [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 61,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 61,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 62,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 62,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 63,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 63,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 64,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 64,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 65,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 65,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 66,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 66,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 67,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 67,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 68,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 68,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 69,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 69,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 70,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 70,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 71,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 71,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 72,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 72,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 73,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 73,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 74,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 74,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 75,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 75,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 76,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 76,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 77,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 77,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 78,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 78,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 79,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 79,
            ], [
                'image' => 'default/product/init/tape.png',
                'caption' => 1,
                'product_id' => 80,
            ],
            [
                'image' => 'default/product/init/tape1.png',
                'caption' => 0,
                'product_id' => 80,
            ],
            
            
            
            [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 81,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 81,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 82,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 82,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 83,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 83,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 84,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 84,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 85,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 85,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 86,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 86,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 87,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 87,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 88,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 88,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 89,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 89,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 90,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 90,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 91,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 91,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 92,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 92,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 93,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 93,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 94,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 94,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 95,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 95,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 96,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 96,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 97,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 97,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 98,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 98,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 99,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 99,
            ], [
                'image' => 'default/product/init/generator.png',
                'caption' => 1,
                'product_id' => 100,
            ],
            [
                'image' => 'default/product/init/generator1.png',
                'caption' => 0,
                'product_id' => 100,
            ],

        ];
        \App\ProductMedia::insert($records);
        echo " 👍\n";
    }
}
