<?php

use Illuminate\Database\Seeder;
Use App\Category;
Use App\ContactUs;
Use App\Admin;
Use App\Setting;
use App\User;
use App\Order;
use App\OrderDetails;
use App\ProductMedia;
use App\Product;
use App\SubCategory;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            SettingSeeder::class,
            CategorySeeder::class,
            SubCategorySeeder::class,
            ProductSeeder::class,
            ProductMediaSeeder::class
        ]);
        // factory(Category::class, 30)->create();
        factory(Admin::class, 1)->create();



        // factory(Category::class, 30)->create()->each(function ($user) {
        //     $user->shops()->saveMany(factory(Shop::class, 20)->make());
        // });




        // factory(User::class, 5)->create();
        factory(ContactUs::class, 5)->create();


        factory(User::class, 5)->create();

        // factory(Category::class, 10)->create()->each(function($sub){
        //     $sub->subCategories()->saveMany(factory(SubCategory::class, 5)->make());
        // });

        // factory(Product::class, 50)->create()->each(function($media){
        //     $media->media()->saveMany(factory(ProductMedia::class, 5)->make());
        // });

        // factory(Category::class, 10)->create()->each(function($sub){
        //     $sub->subCategories()->saveMany(factory(SubCategory::class, 5)->create()->each(function($product){
        //         $product->product()->saveMany(factory(Product::class, 20)->make());
        //     }));
        // });

        // factory(Order::class, 10)->create()->each(function($o_deta){
        //     $o_deta->orderDetail()->saveMany(factory(OrderDetails::class, 10)->make());
        // });
        }
        // $this->call(UsersTableSeeder::class);

    }

