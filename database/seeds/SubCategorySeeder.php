<?php

use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedSubCategory();
    }

    private function seedSubCategory()
    {
        $records = [
            [
                'name_ar' => 'كابلات كهربائية ',
                'name_en' => 'Electric Cables',
                'category_id' => 1,
            ],
            [
                'name_ar' => 'كابلات معدنية ',
                'name_en' => 'Metal Cables',
                'category_id' => 1,
            ],
            [
                'name_ar' => 'كابلات نحاسية ',
                'name_en' => 'Copper Cables',
                'category_id' => 1,
            ],
            [
                'name_ar' => 'كابلات بلاستيكية ',
                'name_en' => 'Plastic Cables',
                'category_id' => 1,
            ],

            [
                'name_ar' => 'مشتركات كهربائية ',
                'name_en' => 'Electric Joints',
                'category_id' => 2,
            ],
            [
                'name_ar' => 'مشتركات معدنية ',
                'name_en' => 'Metal Joints',
                'category_id' => 2,
            ],
            [
                'name_ar' => 'مشتركات نحاسية ',
                'name_en' => 'Copper Joints',
                'category_id' => 2,
            ],
            [
                'name_ar' => 'مشتركات بلاستيكية ',
                'name_en' => 'Plastic Joints',
                'category_id' => 2,
            ],

            [
                'name_ar' => 'مفكات كهربائية ',
                'name_en' => 'Electric screwdrivers',
                'category_id' => 2,
            ],
            [
                'name_ar' => 'مفكات معدنية ',
                'name_en' => 'Metal screwdrivers',
                'category_id' => 3,
            ],
            [
                'name_ar' => 'مفكات نحاسية ',
                'name_en' => 'Copper screwdrivers',
                'category_id' => 3,
            ],
            [
                'name_ar' => 'مفكات بلاستيكية ',
                'name_en' => 'Plastic screwdrivers',
                'category_id' => 3,
            ],



            [
                'name_ar' => 'شرايط كهربائية ',
                'name_en' => 'Electric tapes',
                'category_id' => 4,
            ],
            [
                'name_ar' => 'شرايط معدنية ',
                'name_en' => 'Metal tapes',
                'category_id' => 4,
            ],
            [
                'name_ar' => 'شرايط نحاسية ',
                'name_en' => 'Copper tapes',
                'category_id' => 4,
            ],
            [
                'name_ar' => 'شرايط بلاستيكية ',
                'name_en' => 'Plastic tapes',
                'category_id' => 4,
            ],
          

            [
                'name_ar' => 'محولات كهربائية ',
                'name_en' => 'Electric Generators',
                'category_id' => 5,
            ],
            [
                'name_ar' => ' محولات كهربائية معدنية',
                'name_en' => 'Metal Electric Generators ',
                'category_id' => 5,
            ],
            [
                'name_ar' => ' محولات كهربائية نحاسية',
                'name_en' => 'Copper Electric Generators ',
                'category_id' => 5,
            ],
            [
                'name_ar' => ' محولات كهربائية بلاستيكية',
                'name_en' => 'Plastic Electric Generators ',
                'category_id' => 5,
            ],
     
        ];
        \App\SubCategory::insert($records);
        echo " 👍\n";
    }
}
