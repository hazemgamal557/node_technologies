<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('confirm_code')->nullable();
            $table->string('address')->nullable();
            $table->text('image')->nullable();
            $table->boolean('activated_by_code')->default(false);
            $table->boolean('blocked')->default(false);
            $table->string('request_new_phone')->nullable();
            $table->timestamp('request_confirm_code_date')->nullable();
            $table->string('role')->default('user')->comment('user|admin');
            $table->string('name');
            $table->string('phone');
            $table->string('lang')->default('ar');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
